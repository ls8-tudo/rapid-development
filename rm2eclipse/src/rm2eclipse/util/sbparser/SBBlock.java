/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla - initial API and implementation
 ******************************************************************************/

package rm2eclipse.util.sbparser;

import java.util.ArrayList;
import java.util.List;

/**
 * A (named or unnamed) block in a simple block structure document. Can contain
 * more {@link SBElement SBElements}. Keeps track of indentation and line breaks
 * when getting a string representation of this object.
 *
 * @author Jan Czogalla
 *
 */
public class SBBlock extends SBElement {

	String name;
	List<SBElement> children = new ArrayList<SBElement>();
	int length;

	@Override
	public void toString(StringBuilder sb, int indent) {
		// TODO line breaks vs blocksize
		for (int i = 0; i < indent; i++) {
			sb.append('\t');
		}
		if (name != null && !name.isEmpty()) {
			sb.append(name);
			sb.append(' ');
		}
		sb.append('{');
		if (length > 1) {
			sb.append('\n');
		}
		SBElement last = this;
		boolean makeBreak;
		for (SBElement pe : children) {
			makeBreak = last.offset != pe.offset;
			pe.toString(sb, makeBreak ? indent + 1 : 0);
			if (makeBreak) {
				sb.append('\n');
			}
			last = pe;
		}
		if (length > 1) {
			for (int i = 0; i < indent; i++) {
				sb.append('\t');
			}
		}
		sb.append("}");
	}

}
