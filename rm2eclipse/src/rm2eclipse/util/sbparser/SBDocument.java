/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla - initial API and implementation
 ******************************************************************************/

package rm2eclipse.util.sbparser;

import java.util.ArrayList;
import java.util.List;

/**
 * An {@link SBDocument} represents a simple block structure document, comprised
 * of named and unnamed blocks, instructions and comments. Blocks begin with a
 * "{" and end with a "}"; if there is text in front of the "{", it will become
 * a named block with the aforementioned text as the name. Instructions are
 * simple non-block texts. Comments are lines that start with "//", "/*" or "*".
 *
 * @author Jan Czogalla
 *
 */
public class SBDocument {

	List<SBElement> elements = new ArrayList<SBElement>();

	/**
	 * Returns the named block specified by the given path. Returns null, if no
	 * such block exists. Nested blocks are specified via "." delimited named
	 * blocks they are surrounded by, e.g. the path this.is.inner refers to the
	 * block "inner" that is nested inside the "is" block which again is
	 * enclosed by the "this" block.
	 *
	 * @param path
	 *            The path of the named block.
	 * @return The corresponding named block or null.
	 */
	private SBBlock getBlock(String path) {
		List<SBElement> search = new ArrayList<SBElement>(elements);
		String[] split;
		if (path == null || path.isEmpty()) {
			split = new String[0];
		} else if (path.contains(".")) {
			split = path.split("[.]");
		} else {
			split = new String[] { path };
		}
		boolean found;
		SBBlock b = null;
		for (String block : split) {
			found = false;
			for (SBElement pe : search) {
				if (!(pe instanceof SBBlock)) {
					continue;
				}
				b = (SBBlock) pe;
				if (b.name.equals(block)) {
					found = true;
					search.clear();
					search.addAll(b.children);
					break;
				}
			}
			if (!found) {
				return null;
			}
		}
		return b;
	}

	/**
	 * Checks whether the named block exists in this document.
	 *
	 * @param path
	 *            The path of the named block.
	 * @return <tt>true</tt> if the named block exists in this document.
	 * @see #getBlock(String)
	 */
	public boolean hasBlock(String path) {
		return getBlock(path) != null;
	}

	/**
	 * Searches for "key=value" instructions in this document under the given
	 * path and returns the value part if the key is found. If the key could not
	 * be found, null is returned.
	 *
	 * @param path
	 *            The path of the enclosing block or null if the key should be
	 *            searched in the document directly
	 * @param key
	 *            The key to search for.
	 * @return The value of the specified key or null.
	 */
	public String getValue(String path, String key) {
		List<SBElement> search;
		if (path != null && !path.isEmpty()) {
			SBBlock b = getBlock(path);
			if (b == null) {
				return null;
			}
			search = new ArrayList<SBElement>(b.children);
		} else {
			search = new ArrayList<SBElement>(elements);
		}
		String instr;
		int eq, sp;
		String[] split;
		for (SBElement pe : search) {
			if (!(pe instanceof SBInstruction)) {
				continue;
			}
			instr = ((SBInstruction) pe).instruction;
			eq = instr.indexOf('=');
			sp = instr.indexOf(' ');
			// eq == sp => eq==-1 && sp==-1
			if (eq == sp) {
				continue;
			}
			split = instr.split(eq >= 0 ? "=" : " ", 2);
			if (!split[0].contains(key)) {
				continue;
			}
			return split[1].trim();
		}
		return null;
	}

	/**
	 * Searches for instructions that contain all of the specified search
	 * strings in this document under the given path and returns the whole
	 * instruction as a string.
	 *
	 * @param path
	 *            The path of the enclosing block or null if the instruction
	 *            should be searched in the document directly
	 * @param searchStrings
	 *            A number of search strings that should be found in an
	 *            instruction.
	 * @return The instruction as a string or null if no match was found.
	 */
	public String uses(String path, String... searchStrings) {
		List<SBElement> search;
		if (path != null && !path.isEmpty()) {
			SBBlock b = getBlock(path);
			if (b == null) {
				return null;
			}
			search = new ArrayList<SBElement>(b.children);
		} else {
			search = new ArrayList<SBElement>(elements);
		}
		String instruction;
		outer: for (SBElement pe : search) {
			if (!(pe instanceof SBInstruction)) {
				continue;
			}
			instruction = ((SBInstruction) pe).instruction;
			if (instruction.contains("=")) {
				continue;
			}
			for (String searchString : searchStrings) {
				if (!instruction.contains(searchString)) {
					continue outer;
				}
			}
			return instruction;
		}
		return null;
	}

	/**
	 * Returns a somewhat compressed string representation of the original
	 * document, as empty lines are skipped. Otherwise tries to maintain
	 * indentation and line breaks.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		return toString(sb);
	}

	/**
	 * Builds the string representation of this document with the provided
	 * {@link StringBuilder} to avoid unnecessary memory consumption.
	 *
	 * @param sb
	 *            The provided string builder.
	 * @return a String representing the original document.
	 */
	public String toString(StringBuilder sb) {
		SBElement last = null;
		boolean makeBreak;
		for (SBElement pe : elements) {
			makeBreak = last == null || last.offset != pe.offset;
			pe.toString(sb, 0);
			if (makeBreak) {
				sb.append('\n');
			}
			last = pe;
		}
		return sb.toString();
	}

}
