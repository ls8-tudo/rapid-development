/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla - initial API and implementation
 ******************************************************************************/

package rm2eclipse.util.sbparser;

/**
 * A comment in a simple block structure document. A container for a string and
 * a line number.
 *
 * @author Jan Czogalla
 *
 */
public class SBComment extends SBElement {

	String comment;

	@Override
	public void toString(StringBuilder sb, int indent) {
		for (int i = 0; i < indent; i++) {
			sb.append('\t');
		}
		sb.append(comment);
	}

}
