/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla - initial API and implementation
 ******************************************************************************/

package rm2eclipse.util.sbparser;

/**
 * The abstract super class of all simple structure document elements.
 *
 * @author Jan Czogalla
 *
 */
public abstract class SBElement {

	int offset;

	/**
	 * Builds the string representation of this document with the provided
	 * {@link StringBuilder} to avoid unnecessary memory consumption.
	 *
	 * @param sb
	 *            The provided string builder.
	 * @return a String representing this object.
	 */
	abstract void toString(StringBuilder sb, int indent);

}
