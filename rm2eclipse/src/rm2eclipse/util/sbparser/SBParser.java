/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla - initial API and implementation
 ******************************************************************************/

package rm2eclipse.util.sbparser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Stack;

/**
 * A Parser for simple block documents (in short SB). Can parse from a
 * {@link File} or directly from an {@link InputStream}.
 *
 * @author Jan Czogalla
 * @see SBDocument
 */
public class SBParser {

	private File source;
	private InputStream streamSource;

	/**
	 * Constructs a new {@link SBParser} with a {@link File} as a source.
	 *
	 * @param f
	 *            The file to parse from.
	 * @throws NullPointerException
	 *             If the specified {@link File} is null.
	 */
	public SBParser(File f) {
		if (f == null) {
			throw new NullPointerException("File can not be null.");
		}
		source = f;
	}

	/**
	 * Constructs a new {@link SBParser} with an {@link InputStream} as a
	 * source.
	 *
	 * @param stream
	 *            The stream to parse from.
	 * @throws NullPointerException
	 *             If the specified {@link InputStream} is null.
	 */
	public SBParser(InputStream stream) {
		if (stream == null) {
			throw new NullPointerException("Inputstream can not be null.");
		}
		streamSource = stream;
	}

	/**
	 * Parses the given source to create an {@link SBDocument} representing the
	 * simple block structure of the document.
	 *
	 * @return The parsed document.
	 * @throws IOException
	 *             If an I/O error occurs during reading.
	 * @throws Exception
	 *             If a mismatched "{" or "}" occurs.
	 */
	public SBDocument parse() throws Exception {
		SBDocument main = new SBDocument();
		Reader r;
		BufferedReader br;
		if (source != null) {
			r = new FileReader(source);
		} else {
			r = new InputStreamReader(streamSource);
		}
		br = new BufferedReader(r);
		String line, tmpS;
		int lineNr = -1;
		SBBlock tmpB;
		SBInstruction tmpI;
		SBComment tmpC;
		Stack<SBBlock> blockStack = new Stack<SBBlock>();
		int oPos, cPos, type;
		while ((line = br.readLine()) != null) {
			lineNr++;
			line = line.trim();
			if (line.isEmpty()) {
				continue;
			}
			if (line.startsWith("//") || line.startsWith("/*")
					|| line.startsWith("*")) {
				// read comment
				tmpC = new SBComment();
				if (blockStack.empty()) {
					main.elements.add(tmpC);
				} else {
					blockStack.peek().children.add(tmpC);
				}
				tmpC.comment = line;
				tmpC.offset = lineNr;
				continue;
			}
			process: while (true) {
				oPos = line.indexOf('{');
				cPos = line.indexOf('}');
				type = (oPos == -1 ? 0 : 2) + (cPos == -1 ? 0 : 1);
				if (type > 2 && oPos < cPos) {
					type++;
				}
				switch (type) {
				case 0:
				case 1:
				case 3:
					if (!line.startsWith("}")) {
						// instruction
						if (type == 0) {
							tmpS = line;
						} else {
							tmpS = line.substring(0, cPos).trim();
							line = line.substring(cPos).trim();
							cPos = 0;
						}
						if (!tmpS.isEmpty()) {
							tmpI = new SBInstruction();
							tmpI.offset = lineNr;
							if (blockStack.empty()) {
								main.elements.add(tmpI);
							} else {
								blockStack.peek().children.add(tmpI);
							}
							tmpI.instruction = tmpS;
						}
						// instruction string, no more blocks
						if (type == 0) {
							break process;
						}
					}
					// close block
					if (blockStack.empty()) {
						br.close();
						r.close();
						throw new Exception("Mismatched \"}\" in line "
								+ lineNr);
					}
					tmpB = blockStack.pop();
					tmpB.length = lineNr - tmpB.offset + 1;
					// cut line
					line = line.substring(cPos + 1).trim();
					break;
				case 2:
				case 4:
					// open block
					tmpB = new SBBlock();
					tmpB.offset = lineNr;
					tmpS = line.substring(0, oPos).trim();
					if (!tmpS.isEmpty()) {
						tmpB.name = tmpS;
					}
					// embed block
					if (blockStack.empty()) {
						main.elements.add(tmpB);
					} else {
						blockStack.peek().children.add(tmpB);
					}
					blockStack.push(tmpB);
					// cut line
					line = line.substring(oPos + 1).trim();
					break;
				default:
				}
			}

		}
		if (!blockStack.isEmpty()) {
			br.close();
			r.close();
			throw new Exception("Mismatched \"{\", " + blockStack.size()
					+ " blocks are not closed.");
		}
		br.close();
		r.close();
		return main;
	}

}
