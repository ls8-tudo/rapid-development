/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla - initial API and implementation
 ******************************************************************************/

package rm2eclipse.util;

import static ide2rapidminer.interfaces.Constants.ANT_EXT_NAME;
import static ide2rapidminer.interfaces.Constants.ANT_EXT_NAMESPACE;
import static ide2rapidminer.interfaces.Constants.ANT_EXT_REVISION;
import static ide2rapidminer.interfaces.Constants.ANT_EXT_UPDATE;
import static ide2rapidminer.interfaces.Constants.ANT_EXT_VERSION;
import static ide2rapidminer.interfaces.Constants.ANT_PROPERTY;

import java.io.InputStream;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.resources.IFile;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * A collection of ant utility functions.
 *
 * @author Jan Czogalla
 *
 */

public class AntUtils {

	/**
	 * Expands the given string by replacing variables that can be found in the
	 * node list.
	 *
	 * @param content
	 *            - the string to be expanded.
	 * @param properties
	 *            - the possible variables.
	 * @return the expanded String.
	 */
	public static String expand(String content, NodeList properties) {
		if (!content.contains("${")) {
			return content;
		}
		int start, end;
		String var, resolved;
		while (content.contains("${")) {
			end = content.indexOf('}');
			start = content.lastIndexOf("${", end);
			var = content.substring(start + 2, end);
			resolved = resolve(var, properties);
			content = content.substring(0, start) + resolved
					+ content.substring(end + 1);
		}
		return content;
	}

	/**
	 * Resolves a single variable. Will return the empty string if the variable
	 * could not be found.
	 *
	 * @param var
	 *            - the variable to be resolved.
	 * @param properties
	 *            - the properties where the value of the variable might be
	 *            found.
	 * @return the value of the variable if it was found or the empty string.
	 */
	public static String resolve(String var, NodeList properties) {
		Node property, name;
		NamedNodeMap att;
		for (int i = 0; i < properties.getLength(); i++) {
			property = properties.item(i);
			att = property.getAttributes();
			name = att.getNamedItem("name");
			if (name != null && name.getNodeValue().equals(var)) {
				Node value = att.getNamedItem("value");
				if (value == null) {
					value = att.getNamedItem("location");
				}
				return value == null ? "" : expand(value.getNodeValue(),
						properties);
			}
		}
		return "";
	}

	/**
	 * Looks up the extension id and the name of the jar file of a RapidMiner
	 * extension specified by its build file.
	 *
	 * @param antBuildFile
	 *            - the build file of the extension.
	 * @param antPropertyFile
	 *            - the property file associated with the build file.
	 * @param names
	 *            - optional array to hold the results.
	 * @return an array holding the extension id and the name of the jar file.
	 * @throws Exception
	 *             - if the build file is corrupt or a parsing issue occurred.
	 */
	public static String[] getNamesFromAnt(IFile antBuildFile,
			IFile antPropertyFile, String[] names) throws Exception {
		DocumentBuilder db = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();
		Document doc = db.parse(antBuildFile.getContents(true));
		NodeList list = doc.getElementsByTagName(ANT_PROPERTY);
		String extNS = "", extName = "", extLV;
		String extVer = "", extRev = "", extUp = "";
		if (names == null) {
			names = new String[2];
		}
		// find strings throughout the build file
		extNS = resolve(ANT_EXT_NAMESPACE, list);
		extName = resolve(ANT_EXT_NAME, list);
		extVer = resolve(ANT_EXT_VERSION, list);
		extRev = resolve(ANT_EXT_REVISION, list);
		extUp = resolve(ANT_EXT_UPDATE, list);
		// find strings throughout the properties file
		if (antPropertyFile != null && antPropertyFile.exists()) {
			InputStream source = antPropertyFile.getContents();
			Properties properties = new Properties();
			properties.load(source);
			extNS = properties.getProperty(ANT_EXT_NAMESPACE, extNS);
			extName = properties.getProperty(ANT_EXT_NAME, extName);
			extVer = properties.getProperty(ANT_EXT_VERSION, extVer);
			extRev = properties.getProperty(ANT_EXT_REVISION, extRev);
			extUp = properties.getProperty(ANT_EXT_UPDATE, extUp);
			source.close();
		}
		if (extNS.isEmpty() || extName.isEmpty() || extVer.isEmpty()
				|| extRev.isEmpty() || extUp.isEmpty()) {
			// at least one part is missing
			throw new Exception("Extension namespace or version not found.");
		}
		extLV = extVer + "." + extRev + "." + extUp;
		names[0] = "rmx_" + extNS;
		names[1] = "rapidminer-" + extName + "-" + extLV + ".jar";
		return names;
	}

}
