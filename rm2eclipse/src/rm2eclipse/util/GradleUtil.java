/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla - initial API and implementation
 ******************************************************************************/

package rm2eclipse.util;

import static ide2rapidminer.interfaces.Constants.GRADLE_EXT_CONF;
import static ide2rapidminer.interfaces.Constants.GRADLE_EXT_NAME;
import static ide2rapidminer.interfaces.Constants.GRADLE_EXT_NAMESPACE;
import static ide2rapidminer.interfaces.Constants.GRADLE_EXT_VERSION;

import java.io.File;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.util.Properties;

import org.eclipse.core.resources.IFile;

import rm2eclipse.util.sbparser.SBDocument;
import rm2eclipse.util.sbparser.SBParser;

/**
 * A collection of gradle utility functions.
 *
 * @author Jan Czogalla
 *
 */
public class GradleUtil {

	/**
	 * Extracts the name space of an extension from the given gradle build file.
	 *
	 * @param gradleBuildFile
	 *            The gradle build file to search through.
	 * @param names
	 *            The string array instance to store the name space to or null
	 *            to create new instance.
	 * @return The string array instance holding the name space at the first
	 *         index.
	 * @throws Exception
	 *             If either the parsing failed or the name space could not be
	 *             determined.
	 */
	public static String[] getExtNSFromGradle(IFile gradleBuildFile,
			String[] names) throws Exception {
		if (names == null) {
			names = new String[2];
		}
		SBParser parser = new SBParser(gradleBuildFile.getContents());
		SBDocument doc;
		try {
			doc = parser.parse();
		} catch (Exception e) {
			throw new Exception("Could not parse gradle build file of project "
					+ gradleBuildFile.getProject().getName() + ".");
		}
		String extNS = doc.getValue(GRADLE_EXT_CONF, GRADLE_EXT_NAMESPACE);
		if (extNS == null) {
			extNS = doc.getValue(GRADLE_EXT_CONF, GRADLE_EXT_NAME);
			if (extNS == null) {
				throw new Exception(
						"Neither namespace nor name defined for project "
								+ gradleBuildFile.getProject().getName() + ".");
			}
			extNS = extNS.toLowerCase().replace(' ', '_');
		}
		names[0] = "rmx_" + extNS;
		names[0] = names[0].replaceAll("\"|'", "");
		return names;
	}

	/**
	 * Finds the newly build jar file of the extension.
	 *
	 * @param gradleBuildFile
	 *            The gradle build file to search through.
	 * @param gradlePropertyFile
	 *            The gradle property fiel to search through.
	 * @param names
	 *            The string array instance to store the name space to or null
	 *            to create new instance.
	 * @returnThe string array instance holding the jar file location at the
	 *            second index.
	 * @throws Exception
	 *             If either the parsing failed, the version could not be
	 *             determined or the jar file could not be found.
	 */
	public static String[] getExtJarFromGradle(IFile gradleBuildFile,
			IFile gradlePropertyFile, String[] names) throws Exception {
		if (names == null) {
			names = new String[2];
		}
		SBParser parser = new SBParser(gradleBuildFile.getContents());
		SBDocument doc;
		try {
			doc = parser.parse();
		} catch (Exception e) {
			throw new Exception("Could not parse gradle build file of project "
					+ gradleBuildFile.getProject().getName() + ".");
		}
		String extLV = doc.getValue(null, GRADLE_EXT_VERSION);
		if (extLV == null && gradlePropertyFile != null
				&& gradlePropertyFile.exists()) {
			InputStream source = gradlePropertyFile.getContents();
			Properties properties = new Properties();
			properties.load(source);
			extLV = properties.getProperty(GRADLE_EXT_VERSION);
		}
		if (extLV == null) {
			throw new Exception(
					"Extension version not found for RapidMiner extension "
							+ names[0] + ".");
		}

		final String version = extLV.replaceAll("\"|'", "");
		File libsFolder = gradleBuildFile.getProject().getFile("build/libs/")
				.getLocation().toFile();
		File[] candids = libsFolder.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.contains(version);
			}
		});
		File jar = null;
		for (File f : candids) {
			if (jar == null || jar.lastModified() < f.lastModified()) {
				jar = f;
			}
		}
		if (jar == null) {
			throw new Exception(
					"Could not find compiled jar file for RapidMiner extension "
							+ names[0] + ".");
		}
		names[1] = jar.getAbsolutePath();
		return names;
	}

}
