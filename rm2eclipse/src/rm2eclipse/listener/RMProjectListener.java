/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla, Daniel Smit - initial API and implementation
 ******************************************************************************/

package rm2eclipse.listener;

import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.runtime.Path;

import rm2eclipse.RM2EPlugin;

/**
 * Project listener to identify projects that are associated with RapidMiner,
 * especially to find RapidMiner extension projects.
 *
 * @author Jan Czogalla, Daniel Smit
 *
 */
public class RMProjectListener implements IResourceChangeListener {

	private RM2EPlugin plugin;
	private Set<IProject> changedProjects = new HashSet<IProject>();
	private DocumentBuilder db;
	// paths to ignore when checking for changes
	// e.g. class files produced during ant or eclipse build are NOT a change to
	// the extension
	private static String[] ignorePaths = new String[] { "/bin", "/build",
			"/javadoc" };

	private long lastUpdate;
	private Object lock = new Object();

	/**
	 * The constructor
	 */
	public RMProjectListener() {
		this.plugin = RM2EPlugin.getDefault();
		try {
			db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO?!
		}
	}

	@Override
	public void resourceChanged(IResourceChangeEvent change) {
		// ignore changes to special paths that do not contribute to a change in
		// the extension
		if (change.getDelta() != null) {
			IResourceDelta delta = change.getDelta();
			if (delta.getAffectedChildren().length == 0) {
				return;
			}
			// for (ResourceDelta)
			boolean ignore = true;
			int affected, toIgnore;
			for (IResourceDelta subDelta : delta.getAffectedChildren()) {
				affected = subDelta.getAffectedChildren().length;
				toIgnore = 0;
				for (String ignorePath : ignorePaths) {
					toIgnore += subDelta.findMember(new Path(ignorePath)) != null ? 1
							: 0;
				}
				if (toIgnore < affected) {
					ignore = false;
					break;
				}
			}
			if (ignore) {
				return;
			}
		}
		switch (change.getType()) {
		case IResourceChangeEvent.PRE_DELETE:
			preDelete(change);
			break;
		case IResourceChangeEvent.PRE_BUILD:
		case IResourceChangeEvent.PRE_REFRESH:
			preRefresh(change);
		}
		// save time of last update
		lastUpdate = System.currentTimeMillis();
		// keep sync to ensure that all changes were processed before
		// redeploying starts
		synchronized (lock) {
			lock.notify();
		}
	}

	/**
	 * Removes a deleted project from both the extension and the changed
	 * projects.
	 *
	 * @param change
	 *            - the resource change event.
	 */
	private void preDelete(IResourceChangeEvent change) {
		IResource res = change.getResource();
		if (res.equals(plugin.getRMProject())) {
			plugin.setRMProject(null);
			return;
		}
		synchronized (changedProjects) {
			plugin.removeProject(res);
			changedProjects.remove(res);
		}
	}

	/**
	 * Evaluates the changes either on a single project or the whole workspace.
	 *
	 * @param change
	 *            - the resource change event.
	 */
	private void preRefresh(IResourceChangeEvent change) {
		if (change.getSource() instanceof IProject) {
			processProject((IProject) change.getResource(), change.getDelta());
			return;
		}
		synchronized (changedProjects) {
			IResourceDelta delta = change.getDelta();
			if (delta == null) {
				return;
			}
			IResourceDelta[] projects = delta.getAffectedChildren();
			for (IResourceDelta pDelta : projects) {
				if (!(pDelta.getResource() instanceof IProject)) {
					continue;
				}
				processProject((IProject) pDelta.getResource(), pDelta);
			}
		}
	}

	/**
	 * Evaluates the changes of a single project. Checks if the project is
	 * already known as a RapidMiner extension, if it is a (newer) RapidMiner
	 * version or a newly "created" RapidMiner extension, e.g. changes were made
	 * so the project is now recognized as an extension.
	 *
	 * @param proj
	 *            - the altered project.
	 * @param delta
	 *            - the delta associated with the project.
	 */
	private void processProject(IProject proj, IResourceDelta delta) {
		if (delta == null || proj == null) {
			return;
		}
		IProject rm = plugin.getRMProject();
		if (proj.equals(rm) || plugin.setRMProject(proj)) {
			// TODO build RM jar?
			// treat RM project as if it were a RMX, lookup build.xml and build
			// it before any extension; build all extensions afterwards?
			// (only concerns RM 5)
			return;
		}
		IFile buildFile;
		if (plugin.isGradleRMX(proj)) {
			buildFile = plugin.getGradleBuildFile(proj);
		} else {
			buildFile = plugin.getAntBuildFile(proj);
		}
		IResourceDelta buildDelta = null;
		if (buildFile != null) {
			buildDelta = delta.findMember(buildFile.getProjectRelativePath());
		}
		// check if build file changed or did not exist so far
		if (buildFile == null || buildDelta != null) {
			if (!RM2EPlugin.checkRMX(proj, db)) {
				changedProjects.remove(proj);
			} else {
				changedProjects.add(proj);
			}
			// other changes
		} else if (plugin.getProjects().contains(proj)) {
			changedProjects.add(proj);
		}
	}

	/**
	 * Returns the projects that changed since the last build.
	 *
	 * @return the projects which were altered.
	 */
	public Set<IProject> getChangedProjects() {
		synchronized (changedProjects) {
			Set<IProject> set = new HashSet<IProject>(changedProjects);
			changedProjects.clear();
			return set;
		}
	}

	/**
	 * Adds the specified projects to the change list, e.g. after a failed
	 * build.
	 *
	 * @param failedProjects
	 *            The projects that should be handled as changed.
	 */
	public void addChangedProjects(Set<IProject> failedProjects) {
		synchronized (changedProjects) {
			changedProjects.addAll(failedProjects);
		}
	}

	/**
	 * Guarantees that all changes are processed before continuing, as long as
	 * the processing takes no more time than 500 milliseconds.
	 */
	public void waitForChanges() {
		long current = System.currentTimeMillis();
		if (current > lastUpdate) {
			try {
				synchronized (lock) {
					lock.wait(500);
				}
			} catch (InterruptedException e) {
			}
		}
	}

}
