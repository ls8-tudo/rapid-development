/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla, Daniel Smit - initial API and implementation
 ******************************************************************************/

package rm2eclipse.listener;

import ide2rapidminer.interfaces.Constants;
import ide2rapidminer.interfaces.OpenAndActivate;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchesListener2;

import rm2eclipse.RM2EPlugin;

//TODO always start RMI registry regardless of RM launch(?)
// making it possible to connect between regular RM start and eclipse
/**
 * A launch listener that checks if the RapidMiner GUI is running.
 *
 * @author Jan Czogalla, Daniel Smit
 *
 */
public class RMGUIListener implements ILaunchesListener2 {

	private static RMGUIListener instance;

	private Registry reg;
	private ILaunch rmLaunch;
	private OpenAndActivate rmListener = new RMListener();

	/**
	 * The key to get the execution class.
	 */
	private final String KEY_MAIN_TYPE = "org.eclipse.jdt.launching.MAIN_TYPE";

	/**
	 * The class name of the RapidMiner GUI
	 */
	private final String RAPID_MINER_GUI = "com.rapidminer.gui.RapidMinerGUI";
	/**
	 * The class name of the RapidMiner launcher
	 */
	private final String RAPID_MINER_LAUNCHER = "com.rapid_i.Launcher";

	/**
	 * The constructor
	 */
	public RMGUIListener() {
		instance = this;
	}

	/**
	 * Prints some information of the given launch. Including: launch mode,
	 * execution class and configuration.
	 *
	 * @param launch
	 *            - the launch to be printed.
	 */
	@SuppressWarnings("unused")
	private void printLaunch(ILaunch launch) {
		try {
			System.out.println("->" + " ---launch mode: "
					+ launch.getLaunchMode() + " ---Class: "
					+ launch.getClass() + " ---Configuration "
					+ launch.getLaunchConfiguration().getAttributes());
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Checks if the RapidMiner GUI was started. If so, creates an RMI registry
	 * and an open action.
	 *
	 */
	@Override
	public void launchesAdded(ILaunch[] launches) {
		if (!RM2EPlugin.getDefault().isActive() || reg != null) {
			return;
		}
		for (ILaunch launch : launches) {
			// printLaunch(launch);
			try {
				String mainType = launch.getLaunchConfiguration().getAttribute(
						KEY_MAIN_TYPE, "null");
				if (mainType.equals(RAPID_MINER_GUI)
						|| mainType.equals(RAPID_MINER_LAUNCHER)) {
					// System.out.println("It's the RapidMinerGUI.");
					rmLaunch = launch;
					if (reg == null) {
						reg = LocateRegistry
								.createRegistry(Constants.RMI_PORT_RM2E);
					}
					OpenAndActivate oaa = (OpenAndActivate) UnicastRemoteObject
							.exportObject(rmListener, 0);
					reg.rebind(RMListener.OPENER, oaa);
				}
			} catch (CoreException e) {
				e.printStackTrace();
			} catch (RemoteException e) {
				e.printStackTrace();
				return;
			}
		}

	}

	@Override
	public void launchesChanged(ILaunch[] arg0) {
		// Don't care
	}

	@Override
	public void launchesRemoved(ILaunch[] arg0) {
		// Don't care
	}

	/**
	 * Checks if the RapidMiner GUI was terminated. If so, disposes the created
	 * RMI registry and open action.
	 *
	 */
	@Override
	public void launchesTerminated(ILaunch[] launches) {
		if (rmLaunch == null) {
			return;
		}
		for (ILaunch launch : launches) {
			if (!launch.equals(rmLaunch)) {
				continue;
			}
			rmLaunch = null;
			try {
				reg.unbind(RMListener.OPENER);
				UnicastRemoteObject.unexportObject(rmListener, true);
				UnicastRemoteObject.unexportObject(reg, true);
			} catch (NullPointerException e) {
				// second terminate, ignore
			} catch (Exception e) {
				e.printStackTrace();
			}
			reg = null;
		}
	}

	/**
	 * Returns the current instance.
	 *
	 * @return the current instance.
	 */
	public static RMGUIListener getDefault() {
		return instance;
	}

	/**
	 * Returns whether the RapidMiner GUI is running at this moment.
	 *
	 * @return <tt>true</tt> if RapidMiner is running.
	 */
	public boolean isRunning() {
		return rmLaunch != null;
	}

}
