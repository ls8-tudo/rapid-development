/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla - initial API and implementation
 ******************************************************************************/

package rm2eclipse.listener;

import static rm2eclipse.util.AntUtils.getNamesFromAnt;
import static rm2eclipse.util.GradleUtil.getExtNSFromGradle;
import ide2rapidminer.interfaces.OpenAndActivate;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import rm2eclipse.RM2EPlugin;

/**
 * The eclipse implementation for the OpenAndActivate interface. It uses the
 * eclipse specific {@link IFile} resource to find a class file and invokes an
 * eclipse editor associated with java files.
 *
 * @see IDE#openEditor(IWorkbenchPage, IFile)
 * @see IProject#getFile(String)
 *
 * @author Jan Czogalla
 *
 */
public class RMListener implements OpenAndActivate {

	@Override
	public boolean open(String clazz) throws RemoteException {
		return open(clazz, null);
	}

	@Override
	public boolean open(String clazz, String id) throws RemoteException {
		// Find out if clazz exists (in workspace)
		final IFile file = findFileForClass(clazz, id);
		if (file == null) {
			return false;
		}
		final boolean[] success = new boolean[] { false };
		// open java editor with the given file; needs to be a GUI thread.
		Display.getDefault().syncExec(new Runnable() {
			public void run() {
				IWorkbench wb = PlatformUI.getWorkbench();
				IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
				if (win == null) {
					return;
				}
				IWorkbenchPage page = win.getActivePage();
				IEditorPart editor;
				try {
					editor = IDE.openEditor(page, file, true);
				} catch (PartInitException e) {
					return;
				} catch (Exception e2) {
					e2.printStackTrace();
					return;
				}
				editor.setFocus();
				success[0] = true;
				// restore eclipse shell
				win.getShell().setMinimized(false);
			}
		});
		return success[0];
	}

	/**
	 * Finds the java file for a given class and extension ID. Expects a fully
	 * qualified class name and searches through the extension projects and the
	 * RapidMiner project to find that file. Will return null if the file could
	 * not be found.
	 *
	 * @param clazz
	 *            - the fully qualified class name.
	 * @param id
	 *            - the RapidMiner extension id or null.
	 * @return the IFile resource holding the code of the given class or null if
	 *         it could not be found.
	 */
	private IFile findFileForClass(String clazz, String id) {
		String filename = clazz.replaceAll("[.]", "/");
		filename = filename + ".java";
		IFile file = null;
		// extension projects
		Set<IProject> projects;
		if (id == null || id.isEmpty()) {
			projects = new HashSet<IProject>(RM2EPlugin.getDefault()
					.getProjects());
		} else {
			// only project with correct ID
			projects = new HashSet<IProject>();
			for (IProject proj : RM2EPlugin.getDefault().getProjects()) {
				if (RM2EPlugin.getDefault().isGradleRMX(proj)) {
					if (checkGradleProjectForID(proj, id)) {
						filename = "src/main/java/" + filename;
						projects.add(proj);
						break;
					}
				} else {
					if (checkAntProjectForID(proj, id)) {
						filename = "src/" + filename;
						projects.add(proj);
						break;
					}
				}
			}
		}
		// add RM project
		projects.add(RM2EPlugin.getDefault().getRMProject());
		boolean wasClosed;
		for (IProject proj : projects) {
			if (!proj.exists()) {
				continue;
			}
			wasClosed = !proj.isOpen();
			if (wasClosed) {
				try {
					proj.open(null);
				} catch (CoreException e) {
					continue;
				}
			}
			if (RM2EPlugin.getDefault().isGradleRMX(proj)) {
				file = proj.getFile("src/main/java/" + filename);
			} else {
				file = proj.getFile("src" + filename);
			}
			if (file.exists()) {
				return file;
			}
			// restore project state if necessary
			if (wasClosed) {
				try {
					proj.close(null);
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**
	 * Checks whether the given gradle based RapidMiner extension project
	 * matches the given extension id.
	 *
	 * @param proj
	 *            The project to check.
	 * @param id
	 *            The id of the extension to check for.
	 * @return <tt>true</tt> if the project matches the id.
	 */
	private boolean checkGradleProjectForID(IProject proj, String id) {
		IFile gradleBuildFile = RM2EPlugin.getDefault()
				.getGradleBuildFile(proj);
		String extNS;
		try {
			extNS = getExtNSFromGradle(gradleBuildFile, null)[0];
		} catch (Exception e) {
			return false;
		}
		return extNS.endsWith(id);
	}

	/**
	 * Checks whether the given ant based RapidMiner extension project matches
	 * the given extension id.
	 *
	 * @param proj
	 *            The project to check.
	 * @param id
	 *            The id of the extension to check for.
	 * @return <tt>true</tt> if the project matches the id.
	 */
	private boolean checkAntProjectForID(IProject proj, String id) {
		IFile buildFile = RM2EPlugin.getDefault().getAntBuildFile(proj);
		String extNS;
		try {
			extNS = getNamesFromAnt(buildFile, null, null)[0];
		} catch (Exception e) {
			return false;
		}
		return extNS.endsWith(id);
	}
}