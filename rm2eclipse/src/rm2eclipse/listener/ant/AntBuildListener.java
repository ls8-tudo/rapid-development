/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla, Daniel Smit - initial API and implementation
 ******************************************************************************/

package rm2eclipse.listener.ant;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchesListener2;

/**
 * A reusable listener for RapidMiner extension ant builds. Will check if a
 * terminated ant build was successful.
 *
 * @author Jan Czogalla, Daniel Smit
 *
 */
public class AntBuildListener implements ILaunchesListener2 {

	private Object listenerLock;
	private ILaunch current;
	private AntConsoleListener acl;
	private boolean success;

	/**
	 * The constructor. Registers a lock object and the ant console listener
	 *
	 * @param listenerLock
	 *            - the lock object for synchronization.
	 * @param acl
	 *            - the ant console listener.
	 */
	public AntBuildListener(Object listenerLock, AntConsoleListener acl) {
		this.listenerLock = listenerLock;
		this.acl = acl;
	}

	/**
	 * Sets the launch this listener should look for.
	 *
	 * @param launch
	 *            - the launch to listen on.
	 */
	public void setLaunch(ILaunch launch) {
		current = launch;
	}

	@Override
	public void launchesRemoved(ILaunch[] launches) {
		// Don't care
	}

	@Override
	public void launchesAdded(ILaunch[] launches) {
		// Don't care
	}

	@Override
	public void launchesChanged(ILaunch[] launches) {
		// Don't care
	}

	/**
	 * Checks the given array for the ant build and checks if it was successful.
	 * Synchronizes with the redeploy action.
	 */
	@Override
	public void launchesTerminated(ILaunch[] launches) {
		for (int i = 0; i < launches.length; i++) {
			if (launches[i].equals(current)) {
				// success if no problems occurred or the only problem was
				// moving the jar-file
				success = (acl.succeeded() || acl.needsMoving())
						&& !((IProgressMonitor) current.getProcesses()[0])
								.isCanceled();
				synchronized (listenerLock) {
					listenerLock.notify();
				}
				return;
			}
		}
	}

	/**
	 * Returns <tt>true</tt> if the ant build was successful.
	 *
	 * @return <tt>true</tt> upon success.
	 */
	public boolean isSuccessful() {
		return success;
	}

}
