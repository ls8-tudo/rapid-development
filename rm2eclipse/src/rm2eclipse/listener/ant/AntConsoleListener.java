/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla, Daniel Smit - initial API and implementation
 ******************************************************************************/

package rm2eclipse.listener.ant;

import ide2rapidminer.interfaces.Constants;

import org.eclipse.debug.core.IStreamListener;
import org.eclipse.debug.core.model.IStreamMonitor;

/**
 * A console listener to catch if an Ant build failed.
 *
 * @author Jan Czogalla, Daniel Smit
 *
 */
public class AntConsoleListener implements IStreamListener {

	private boolean success = true;
	private boolean toMove = false;

	/**
	 * Checks the appended text if the running Ant build failed. If it failed,
	 * will check if the created jar file just has to be moved.
	 */
	@Override
	public void streamAppended(String text, IStreamMonitor monitor) {
		if (text.indexOf(Constants.ANT_FAILED) > -1) {
			success = false;
		}
		if (!success && text.indexOf(Constants.ANT_NOT_MOVED) > -1) {
			toMove = true;
		}
	}

	/**
	 * Resets this listener.
	 */
	public void reset() {
		success = true;
		toMove = false;
	}

	/**
	 * Returns whether the Ant build was successful.
	 *
	 * @return <tt>true</tt> if the Ant build succeeded.
	 */
	public boolean succeeded() {
		return success;
	}

	/**
	 * Returns whether the jar file was created but needs to be moved.
	 *
	 * @return <tt>true</tt> if the jar file needs to be moved.
	 */
	public boolean needsMoving() {
		return toMove;
	}

}
