/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla, Daniel Smit - initial API and implementation
 ******************************************************************************/

package rm2eclipse.actions;

import static ide2rapidminer.interfaces.Constants.ANT_BUILD_FILE_SUFFIX;
import static ide2rapidminer.interfaces.Constants.GRADLE_PROPERTY_FILE;
import static ide2rapidminer.interfaces.Constants.RMI_PORT_IDE2RM;
import static rm2eclipse.util.AntUtils.getNamesFromAnt;
import static rm2eclipse.util.GradleUtil.getExtJarFromGradle;
import static rm2eclipse.util.GradleUtil.getExtNSFromGradle;
import ide2rapidminer.interfaces.ReloadOperators;

import java.io.File;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.ant.launching.IAntLaunchConstants;
import org.eclipse.core.externaltools.internal.IExternalToolConstants;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.MultiRule;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.RefreshUtil;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressConstants;
import org.springsource.ide.eclipse.gradle.core.GradleProject;
import org.springsource.ide.eclipse.gradle.core.TaskUtil;
import org.springsource.ide.eclipse.gradle.core.launch.GradleLaunchConfigurationDelegate;
import org.springsource.ide.eclipse.gradle.core.modelmanager.DefaultModelBuilder;
import org.springsource.ide.eclipse.gradle.core.modelmanager.GradleModelManager;
import org.springsource.ide.eclipse.gradle.core.util.JobUtil;

import rm2eclipse.RM2EPlugin;
import rm2eclipse.listener.RMGUIListener;
import rm2eclipse.listener.ant.AntBuildListener;
import rm2eclipse.listener.ant.AntConsoleListener;

/**
 * The redeploy action of the rm2e extension.
 *
 * @author Jan Czogalla, Daniel Smit
 *
 */
public class Redeploy extends Job implements IWorkbenchWindowActionDelegate {
	/**
	 * Flag to show an information message.
	 */
	public static final int INFO_STYLE = 0;
	/**
	 * Flag to show an error message.
	 */
	public static final int ERROR_STYLE = 1;
	/**
	 * Flag to show a confirmation dialog.
	 */
	public static final int CONFIRM_STYLE = 2;

	// listeners and RMI objects
	private ILaunchManager launchManager;
	private Object listenerLock;
	private AntConsoleListener acl;
	private AntBuildListener abl;
	private ReloadOperators rlOp;
	private Exception rmi;

	// monitoring objects
	private IWorkbenchWindow window;
	private IProgressMonitor monitor;
	private Set<IProject> toBuild;
	private boolean RMrunning;

	/**
	 * The constructor.
	 */
	public Redeploy() {
		super("Redeploy Extensions");
		setUser(true);
	}

	/**
	 * Builds the extensions that changed and tells RapidMiner which extensions
	 * to reload. If RapidMiner is not running at this time, extensions will
	 * only be built. This method does nothing if the rm2e extension is not
	 * active.
	 */
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		this.monitor = monitor;
		// if extension inactive, do nothing
		if (!RM2EPlugin.getDefault().isActive()) {
			showMessage("rm2e not active",
					"There is no RapidMiner in this workspace.");
			monitor.done();
			return Status.OK_STATUS;
		}

		RMrunning = RMGUIListener.getDefault().isRunning();

		// get projects to build and return if there are none
		if (toBuild.isEmpty()) {
			showMessage("No extensions to build",
					"There are no changes in any extension.");
			monitor.done();
			return Status.OK_STATUS;
		}

		if (monitor.isCanceled()) {
			monitor.done();
			return Status.CANCEL_STATUS;
		}
		// set workload; 3 steps for preparation, 10 for each project build,
		// 2 for administration and 1 for each project refresh
		monitor.beginTask("Redeploy extensions", 5 + 11 * toBuild.size());
		monitor.worked(1);
		monitor.subTask("Configure launch environment");

		// get ant launch configuration
		ILaunchConfigurationWorkingCopy workingCopy = getAntLaunchConfig();
		if (workingCopy == null) {
			// TODO?
			showMessage("Launch configuration error",
					"Could not create launch configuration.", ERROR_STYLE);
			monitor.done();
			return Status.OK_STATUS;
		}
		// prepare gradle model manager for creating gradle projects
		GradleModelManager gmm = new GradleModelManager(
				new DefaultModelBuilder());

		if (monitor.isCanceled()) {
			monitor.done();
			return Status.CANCEL_STATUS;
		}
		monitor.worked(1);

		monitor.subTask("Prepare building");
		// create and add ant listeners
		listenerLock = new Object();
		acl = new AntConsoleListener();
		abl = new AntBuildListener(listenerLock, acl);
		launchManager.addLaunchListener(abl);

		// if RapidMiner is running, get the remote object for controlling the
		// reloading of the extensions
		rlOp = null;
		rmi = null;
		if (RMrunning) {
			buildRMI();
		}

		if (monitor.isCanceled()) {
			monitor.done();
			launchManager.removeLaunchListener(abl);
			return Status.CANCEL_STATUS;
		}
		monitor.worked(1);

		// compile each project
		Set<IProject> failed = new HashSet<IProject>();
		Map<String, String> succeeded = new HashMap<String, String>();
		String[] names = new String[2];
		boolean success;
		for (IProject proj : toBuild) {
			names[0] = names[1] = null;
			// TODO do not use launch(config), but internal logics?
			// see GradleProcess => TaskUtil.execute() (done, incl. error
			// detect)
			// AntLaunchDelegate.launch/runinsameVM, AntProcess (not needed?),
			// AntRunner (incl. config from AntLaunchDelegate)
			// advantage: monitor control
			//
			if (RM2EPlugin.getDefault().isGradleRMX(proj)) {
				// TODO build gradle projects in parallel?
				success = buildGradleProject(proj, gmm, names);
			} else {
				// TODO: run compiles simultaneously? would work with fork, but
				// the folder "libfiles" in "release" will prevent a properly
				// working solution (see build_extension.xml)
				success = buildAntProject(proj, workingCopy, names);
			}
			if (success) {
				succeeded.put(names[0], names[1]);
			} else {
				failed.add(proj);
			}
			if (monitor.isCanceled()) {
				monitor.done();
				launchManager.removeLaunchListener(abl);
				return Status.CANCEL_STATUS;
			}
		}

		launchManager.removeLaunchListener(abl);

		monitor.subTask("Gather information");

		StringBuilder report = new StringBuilder();
		if (!failed.isEmpty()) {
			report.append("Could not build the following projects:\n");
			for (IProject proj : failed) {
				report.append('\t');
				report.append(proj.getName());
				report.append('\n');
			}
			report.append('\n');
			RM2EPlugin.getDefault().addChangedProjects(failed);
		}

		if (succeeded.isEmpty()) {
			report.append("Could not build any RapidMiner extensions.");
			showMessage("Building not successful", report.toString(),
					ERROR_STYLE);
			monitor.done();
			return Status.OK_STATUS;
		}

		// refresh all projects
		try {
			RefreshUtil.refreshResources(toBuild.toArray(new IResource[0]),
					IResource.DEPTH_INFINITE, new SubProgressMonitor(monitor,
							toBuild.size()));
		} catch (CoreException e) {
			e.printStackTrace();
		}

		report.append("Built RapidMiner extensions:\n");
		for (String str : succeeded.keySet()) {
			report.append('\t');
			report.append(str);
			report.append('\n');
		}
		report.append('\n');
		if (!RMrunning) {
			// TODO if RM is not running, install as usual! Not only shadowjar
			// how to: for all succeeded projects, call installExtension
			showMessage("Building finished", report.toString());
			monitor.done();
			return Status.OK_STATUS;
		}

		if (monitor.isCanceled()) {
			monitor.done();
			return Status.CANCEL_STATUS;
		}
		monitor.worked(1);
		monitor.subTask("Redeploying");

		if (rlOp == null) {
			report.append("Could not redeploy plugins due to an RMI exception:\n");
			report.append(rmi.getMessage());
			showMessage("Redeploying failed", report.toString(), ERROR_STYLE);
			monitor.done();
			return Status.OK_STATUS;
		}
		if (redeploy(succeeded)) {
			report.append("Redeploying finished, switch to RapidMiner?");
			showMessage("Redeploying successful", report.toString(),
					CONFIRM_STYLE);
		} else {
			showMessage("Redeploying failed", report.toString(), ERROR_STYLE);
		}
		monitor.done();
		return Status.OK_STATUS;
	}

	/**
	 * Creates an Ant launch configuration.
	 *
	 * @return the Ant launch configuration.
	 */
	private ILaunchConfigurationWorkingCopy getAntLaunchConfig() {
		launchManager = DebugPlugin.getDefault().getLaunchManager();
		ILaunchConfigurationType type = launchManager
				.getLaunchConfigurationType(IAntLaunchConstants.ID_ANT_LAUNCH_CONFIGURATION_TYPE);
		ILaunchConfigurationWorkingCopy workingCopy;
		try {
			workingCopy = type.newInstance(null, "RMX Ant Builder");
		} catch (CoreException e) {
			return null;
		}
		workingCopy.setAttribute(ILaunchManager.ATTR_PRIVATE, true);
		return workingCopy;
	}

	/**
	 * Builds the given ant based RapidMiner extension project. Uses the ant
	 * launch configuration and eclipse launch mechanism to execute the build.
	 *
	 * @param proj
	 *            - the project to be built.
	 * @param workingCopy
	 *            - the ant launch configuration to run.
	 * @param names
	 *            - the id and name of the jar file of the extension
	 * @return <tt>true</tt> if the build was successful.
	 */
	private boolean buildAntProject(IProject proj,
			ILaunchConfigurationWorkingCopy workingCopy, String[] names) {
		IFile antBuildFile = RM2EPlugin.getDefault().getAntBuildFile(proj);
		if (antBuildFile == null) {
			return false;
		}
		// idea taken from
		// http://stackoverflow.com/questions/2364247/how-to-run-ant-from-an-eclipse-plugin-send-output-to-an-eclipse-console-and-ca
		IPath loc = antBuildFile.getLocation();
		workingCopy.setAttribute(IExternalToolConstants.ATTR_LOCATION,
				loc.toString());
		acl.reset();

		IFile antPropertyFile = proj.getFile(antBuildFile.getName().replace(
				ANT_BUILD_FILE_SUFFIX, "properties"));
		// get extension id and name of jar file
		try {
			getNamesFromAnt(antBuildFile, antPropertyFile, names);
		} catch (Exception e) {
			// showMessage("Could not resolve extension id and/or name.");
			return false;
		}

		monitor.subTask("Building extension " + names[0]);

		// launch ant build and add listener
		ILaunch launch;
		try {
			launch = workingCopy.launch(ILaunchManager.RUN_MODE,
					new SubProgressMonitor(monitor, 10));
			abl.setLaunch(launch);
		} catch (CoreException e1) {
			return false;
		}
		launch.getProcesses()[0].getStreamsProxy().getErrorStreamMonitor()
				.addListener(acl);
		try {
			// wait for completion of the current build
			synchronized (listenerLock) {
				listenerLock.wait();
			}
		} catch (InterruptedException ie) {
		} catch (Exception e) {
			e.printStackTrace();
		}
		launch.getProcesses()[0].getStreamsProxy().getErrorStreamMonitor()
				.removeListener(acl);
		if (abl.isSuccessful()) {
			if (acl.needsMoving() && !copyJar(names[0])) {
				return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * Builds the given gradle based RapidMiner extension project. Uses the
	 * {@link TaskUtil} to execute the build for better control over
	 * success/fail.
	 *
	 * @param proj
	 *            - the project to be built.
	 * @param gmm
	 *            - the gradle model manager.
	 * @param names
	 *            - the id and name of the jar file of the extension
	 * @return <tt>true</tt> if the build was successful.
	 */
	private boolean buildGradleProject(IProject proj, GradleModelManager gmm,
			String[] names) {
		IFile gradleBuildFile = RM2EPlugin.getDefault()
				.getGradleBuildFile(proj);
		if (gradleBuildFile == null) {
			return false;
		}
		try {
			getExtNSFromGradle(gradleBuildFile, names);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		monitor.subTask("Building extension " + names[0]);

		File projDir = proj.getLocation().toFile();
		GradleProject gProj = new GradleProject(projDir, gmm);

		// ----------------------------
		// using TaskUtil to execute
		// ----------------------------
		// using TaskUtil instead of launch for better control
		String task;
		if (RMrunning) {
			// RM running => only create jar, mark for copy
			task = "shadowjar";
			copyJar(names[0]);
		} else {
			// RM not running => run normal install extension task
			task = "installExtension";
		}
		ILaunchConfiguration glc = GradleLaunchConfigurationDelegate
				.createDefault(gProj, task, false);
		try {
			TaskUtil.execute(gProj, glc,
					GradleLaunchConfigurationDelegate.getTasksList(glc),
					new SubProgressMonitor(monitor, 10));
			IFile gradlePropertyFile = proj.getFile(GRADLE_PROPERTY_FILE);
			getExtJarFromGradle(gradleBuildFile, gradlePropertyFile, names);
			return true;
		} catch (CoreException e) {
			// build failed
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			// did not find jar file
			e.printStackTrace();
			return false;
		}
		// ----------------------------
		// end TaskUtil.execute
		// ----------------------------

		// ----------------------------
		// using launchutil to launch
		// ----------------------------
		// TODO detect "BUILD FAILED" from console
		// GradleProcess gProc;
		// try {
		// gProc = LaunchUtil.launchTasks(gProj, "shadowjar");
		// if (!gProc.isTerminated()) {
		// return false;
		// }
		// IFile gradlePropertyFile = proj.getFile(GRADLE_PROPERTY_FILE);
		// getExtJarFromGradle(gradleBuildFile, gradlePropertyFile, names);
		// return gProc.getExitValue() == 0;
		// } catch (CoreException e) {
		// return false;
		// } catch (Exception e) {
		// return false;
		// }
		// ----------------------------
		// end using launchutil.launch
		// ----------------------------
	}

	/**
	 * Marks the extension to be copied upon redeploy. Returns false only if a
	 * RemoteException occurred.
	 *
	 * @param pluginID
	 *            - the id of the RapidMiner extension.
	 * @return <code>true</code> if the extension was successfully marked.
	 */
	private boolean copyJar(String pluginID) {
		try {
			rlOp.markForCopy(pluginID);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Redeploys the successfully built extensions in RapidMiner. The key of the
	 * given mapping shows which extension id should be replaced and the value
	 * is the name of the jar file that should be loaded.
	 *
	 * @param succeeded
	 *            - the mapping of extension ids to (new) jar files.
	 */
	private boolean redeploy(Map<String, String> succeeded) {
		try {
			if (!rlOp.reload(succeeded)) {
				return false;
			}
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Show an information message. Same as
	 * {@link #showMessage(String, String, int) showMessage(title, message,
	 * INFO_STYLE)}.
	 *
	 * @param title
	 *            - the title of the message.
	 * @param message
	 *            - the message to be shown.
	 */
	private void showMessage(final String title, final String message) {
		showMessage(title, message, INFO_STYLE);
	}

	/**
	 * Show a message with the given style.
	 *
	 * @param title
	 *            - the title of the message.
	 * @param message
	 *            - the message to be shown.
	 * @param style
	 *            - the message Style, one of {@link #INFO_STYLE},
	 *            {@link #ERROR_STYLE} or {@link #CONFIRM_STYLE}.
	 */
	private void showMessage(final String title, final String message,
			final int style) {
		final ReloadOperators reload = rlOp;
		final IAction action = new Action() {
			public void run() {
				switch (style) {
				case INFO_STYLE:
					MessageDialog.openInformation(window.getShell(), title,
							message);
					break;
				case ERROR_STYLE:
					MessageDialog.openError(window.getShell(), title, message);
					break;
				case CONFIRM_STYLE:
					if (MessageDialog.openQuestion(window.getShell(), title,
							message)) {
						Display.getDefault().getActiveShell()
								.setMinimized(true);
						try {
							reload.toFront();
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					}
				}
			}
		};
		if (isModal()) {
			Display.getDefault().asyncExec(new Runnable() {
				public void run() {
					action.run();
				}
			});
		} else {
			setProperty(IProgressConstants.KEEP_PROPERTY, Boolean.TRUE);
			setProperty(IProgressConstants.ACTION_PROPERTY, action);
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// don't care
	}

	@Override
	public void dispose() {
		// don't care
	}

	@Override
	public void init(IWorkbenchWindow window) {
		this.window = window;
	}

	@Override
	public void run(IAction action) {
		// save changes before redeploy
		PlatformUI.getWorkbench().saveAllEditors(true);
		RM2EPlugin.getDefault().waitForChanges();
		toBuild = RM2EPlugin.getDefault().getChangedProjects();
		// make sure the projects are locked during build
		setRule(new MultiRule(new ISchedulingRule[] {
				new MultiRule(toBuild.toArray(new IProject[0])),
				JobUtil.LIGHT_RULE }));
		IJobChangeListener jobListener = new IJobChangeListener() {

			@Override
			public void sleeping(IJobChangeEvent event) {
				// don't care
			}

			@Override
			public void scheduled(IJobChangeEvent event) {
				// don't care
			}

			@Override
			public void running(IJobChangeEvent event) {
				// don't care
			}

			@Override
			public void done(IJobChangeEvent event) {
				setRule(null);
				toBuild = null;
			}

			@Override
			public void awake(IJobChangeEvent event) {
				// don't care
			}

			@Override
			public void aboutToRun(IJobChangeEvent event) {
				// don't care
			}
		};
		addJobChangeListener(jobListener);
		schedule();
	}

	/**
	 * Returns if this action is modal or minimized.
	 *
	 * @return true if this action is modal.
	 */
	private boolean isModal() {
		Boolean isModal = (Boolean) getProperty(IProgressConstants.PROPERTY_IN_DIALOG);
		return isModal == null ? false : isModal;
	}

	/**
	 * Gets the RMI objects.
	 */
	private void buildRMI() {
		try {
			Registry reg = LocateRegistry.getRegistry(RMI_PORT_IDE2RM);
			rlOp = (ReloadOperators) reg.lookup(ReloadOperators.RELOADER);
		} catch (Exception e) {
			rmi = e;
		}
	}
}
