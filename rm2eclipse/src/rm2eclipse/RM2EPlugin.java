/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jan Czogalla, Daniel Smit - initial API and implementation
 ******************************************************************************/

package rm2eclipse;

import static ide2rapidminer.interfaces.Constants.ANT_BUILD_FILE_SUFFIX;
import static ide2rapidminer.interfaces.Constants.ANT_PROPERTY;
import static ide2rapidminer.interfaces.Constants.ANT_RM_DIR;
import static ide2rapidminer.interfaces.Constants.GRADLE_BUILD_FILE;
import static ide2rapidminer.interfaces.Constants.GRADLE_EXT_CONF;
import static ide2rapidminer.interfaces.Constants.GRADLE_RM_PLUGIN;
import static ide2rapidminer.interfaces.Constants.RM_MAIN_CLASS;
import static ide2rapidminer.interfaces.Constants.RM_MIN_VERSION;
import static rm2eclipse.util.AntUtils.resolve;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import rm2eclipse.listener.RMGUIListener;
import rm2eclipse.listener.RMProjectListener;
import rm2eclipse.util.sbparser.SBDocument;
import rm2eclipse.util.sbparser.SBParser;

/**
 * The activator class controls the plug-in life cycle
 *
 * @author Jan Czogalla, Daniel Smit
 */
public class RM2EPlugin extends AbstractUIPlugin implements IStartup {

	/**
	 * The ID of this extension
	 */
	public static final String PLUGIN_ID = "rm2e"; //$NON-NLS-1$
	// The shared instance
	private static RM2EPlugin plugin;

	// TODO distinction between ant and gradle?
	// fields holding the projects and files associated with RapidMiner and its
	// extensions
	private Map<IProject, IFile> antBuildFiles = new HashMap<IProject, IFile>();
	private Map<IProject, IFile> gradleBuildFiles = new HashMap<IProject, IFile>();
	private Map<IProject, Long> lastChange = new HashMap<IProject, Long>();
	// TODO all rm versions?
	private IProject rapidMiner;
	private String rmVersion = "";
	private boolean rmIsGradle;

	private RMProjectListener projListener;

	/**
	 * The constructor
	 */
	public RM2EPlugin() {
	}

	@Override
	public void earlyStartup() {
		// Just so the extension is started at eclipse startup
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		init();
	}

	/**
	 * Initializes the extension and tests if it is active.
	 */
	public void init() {
		// add project listener
		int changeMask = IResourceChangeEvent.PRE_BUILD
				| IResourceChangeEvent.PRE_REFRESH
				| IResourceChangeEvent.PRE_DELETE;
		projListener = new RMProjectListener();
		ResourcesPlugin.getWorkspace().addResourceChangeListener(projListener,
				changeMask);

		// add launch listener
		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
		manager.addLaunchListener(new RMGUIListener());

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		DocumentBuilder db = null;
		try {
			db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// return;
			// no document builder means no ant detection
		}
		boolean closed;
		// check workspace for RapidMiner related projects
		for (IProject proj : workspace.getRoot().getProjects()) {
			closed = false;
			if (!proj.isOpen()) {
				try {
					proj.open(null);
					closed = true;
				} catch (CoreException e) {
					continue;
				}
			}
			// check if the project is RapidMiner
			if (setRMProject(proj)) {
				continue;
			}
			// check if the project is a RapidMiner extension
			checkRMX(proj, db);
			if (closed) {
				try {
					proj.close(null);
				} catch (CoreException e) {
				}
			}
		}
	}

	/**
	 * Tests via a specified xml file if a given project is an ant based
	 * RapidMiner extension.
	 *
	 * @param proj
	 *            - the ant xml file to be tested.
	 * @param db
	 *            - a document builder to evaluate the build file.
	 * @return true if this ant xml file represents a RapidMiner extension.
	 */
	private static boolean isAntRMX(IFile antFile, DocumentBuilder db) {
		// IFile antFile = proj.getFile(Constants.ANT_BUILD_FILE_SUFFIX);
		if (!antFile.exists()) {
			return false;
		}
		System.out.println("Project test: " + antFile.getProject());
		InputStream in;
		try {
			in = antFile.getContents();
		} catch (CoreException e) {
			return false;
		}
		Document doc;
		try {
			doc = db.parse(in);
		} catch (SAXException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		// get ant properties
		NodeList nodes = doc.getElementsByTagName(ANT_PROPERTY);
		if (nodes.getLength() == 0) {
			return false;
		}

		Node node, att;
		// check if the property "rm.dir" exists
		String content = resolve(ANT_RM_DIR, nodes);
		if (content.isEmpty()) {
			return false;
		}

		// check if the build_extension.xml file is imported
		nodes = doc.getElementsByTagName("import");
		for (int i = 0; i < nodes.getLength(); i++) {
			node = nodes.item(i);
			att = node.getAttributes().getNamedItem("file");
			if (att != null) {
				content = att.getNodeValue();
				if (content.equals("${rm.dir}/build_extension.xml")) {
					System.out.println("RMX confirmed.");
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Tests via a specified gradle build file if a given project is a gradle
	 * based RapidMiner extension.
	 *
	 * @param proj
	 *            - the file to be tested.
	 * @return true if this gradle build file represents a RapidMiner extension.
	 */
	private static boolean isGradleRMX(IFile gradleFile) {
		if (!gradleFile.exists()) {
			return false;
		}
		System.out.println("Project test: " + gradleFile.getProject());
		InputStream in;
		try {
			in = gradleFile.getContents();
		} catch (CoreException e) {
			return false;
		}
		SBParser parser = new SBParser(in);
		SBDocument doc;
		try {
			doc = parser.parse();
		} catch (Exception e) {
			return false;
		}
		// check for rm gradle plugin
		if (doc.uses("plugins", "id", GRADLE_RM_PLUGIN, "version") == null) {
			return false;
		}
		// check for "extensionConfig" block
		if (!doc.hasBlock(GRADLE_EXT_CONF)) {
			return false;
		}
		System.out.println("RMX confirmed.");
		return true;
	}

	/**
	 * Tests if a given project is a RapidMiner extension. If the project is a
	 * RapidMiner extension, it will be marked for further lookups and the
	 * method returns true. If the project was a RapidMiner extension and was
	 * altered in a way so it is not anymore, it will be removed from the
	 * context and the method will return false.
	 *
	 * @param proj
	 *            - the project to be tested.
	 * @param db
	 *            - a document builder to evaluate the build file.
	 * @return true if this project is a RapidMiner extension.
	 */
	public static boolean checkRMX(IProject proj, DocumentBuilder db) {
		IResource[] candidates;
		try {
			candidates = proj.members();
		} catch (CoreException e) {
			e.printStackTrace();
			return false;
		}
		IFile buildFile;
		String extension;
		for (IResource res : candidates) {
			if (!(res instanceof IFile)) {
				continue;
			}
			buildFile = (IFile) res;
			extension = buildFile.getFileExtension();
			if (!ANT_BUILD_FILE_SUFFIX.equalsIgnoreCase(extension)
					&& !GRADLE_BUILD_FILE.equals(buildFile.getName())) {
				continue;
			}
			if (isAntRMX(buildFile, db)) {
				plugin.antBuildFiles.put(proj, buildFile);
				if (plugin.lastChange.containsKey(proj)) {
					plugin.lastChange.put(proj, System.currentTimeMillis());
				}
				return true;
			}
			if (isGradleRMX(buildFile)) {
				plugin.gradleBuildFiles.put(proj, buildFile);
				if (plugin.lastChange.containsKey(proj)) {
					plugin.lastChange.put(proj, System.currentTimeMillis());
				}
				return true;
			}
		}
		plugin.removeProject(proj);
		return false;
	}

	/**
	 * Checks whether the given project is a gradle RM(X) project or not.
	 *
	 * @param proj
	 *            The project to be checked.
	 * @return <tt>true</tt> if the project is a gradle RM(X) project
	 */
	public boolean isGradleRMX(IProject proj) {
		if (gradleBuildFiles.containsKey(proj)) {
			return true;
		}
		return proj.equals(rapidMiner) && rmIsGradle;
	}

	/**
	 * Gets the ant build file associated with the given project.
	 *
	 * @param proj
	 *            - the project whose ant file is needed.
	 * @return the ant file associated with the specified project.
	 */
	public IFile getAntBuildFile(IProject proj) {
		return antBuildFiles.get(proj);
	}

	/**
	 * Gets the gradle build file associated with the given project.
	 *
	 * @param proj
	 *            - the project whose gradle file is needed.
	 * @return the gradle file associated with the specified project.
	 */
	public IFile getGradleBuildFile(IProject proj) {
		return gradleBuildFiles.get(proj);
	}

	/**
	 * Gets the time the last time the specified project was last edited.
	 *
	 * @param proj
	 *            - the project to check on.
	 * @return the last time the specified project was changed.
	 */
	public Long getLastChangeTime(IProject proj) {
		return lastChange.get(proj);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework
	 * .BundleContext )
	 */
	public void stop(BundleContext context) throws Exception {
		antBuildFiles.clear();
		lastChange.clear();
		rapidMiner = null;
		plugin = null;
		rmVersion = "";
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(
				projListener);
		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
		manager.removeLaunchListener(RMGUIListener.getDefault());
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static RM2EPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns the projects currently listed as RapidMiner extensions.
	 *
	 * @return the extension projects.
	 */
	public Set<IProject> getProjects() {
		Set<IProject> projects = new HashSet<IProject>();
		projects.addAll(antBuildFiles.keySet());
		projects.addAll(gradleBuildFiles.keySet());
		return projects;
	}

	/**
	 * Removes the given {@link IResource} (should be an {@link IProject}) from
	 * the active RMX list.
	 *
	 * @param res
	 *            The project to be removed.
	 */
	public void removeProject(IResource res) {
		antBuildFiles.remove(res);
		gradleBuildFiles.remove(res);
		plugin.lastChange.remove(res);
	}

	/**
	 * Returns the project currently listed as the newest version of RapidMiner.
	 *
	 * @return the latest RapidMiner project.
	 */
	public IProject getRMProject() {
		return rapidMiner;
	}

	/**
	 * Tests and sets the RapidMiner project or resets it to null.
	 *
	 * @param proj
	 *            - the project to be tested to be the RapidMiner project, or
	 *            null to reset.
	 * @return true if the RapidMiner project was changed (either set or reset).
	 */
	public boolean setRMProject(IProject proj) {
		if (proj == null) {
			rapidMiner = null;
			rmVersion = "";
			return true;
		}

		if (!proj.exists()) {
			return false;
		}

		// ant/gradle divide

		String classFile = RM_MAIN_CLASS.replaceAll("[.]", "/") + ".java";
		boolean isGradle;
		IFile propFile;
		IFile file = proj.getFile("build_extension.xml");
		// ant case
		if (file.exists()) {
			isGradle = false;
			file = proj.getFile("src/" + classFile);
			if (!file.exists()) {
				return false;
			}
			propFile = proj.getFile("build.properties");
			if (!file.exists()) {
				return false;
			}
		} else {
			// gradle case
			file = proj.getFile(GRADLE_BUILD_FILE);
			if (!file.exists()) {
				return false;
			}
			isGradle = true;
			file = proj.getFile("src/main/java/" + classFile);
			if (!file.exists()) {
				return false;
			}
			propFile = proj.getFile("gradle.properties");
			if (!file.exists()) {
				return false;
			}
		}
		// read version from property file
		Properties properties = new Properties();
		InputStream fis = null;
		try {
			fis = propFile.getContents();
			properties.load(fis);
		} catch (IOException ioe) {
		} catch (CoreException ce) {
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
				}
			}
		}

		String version, revision, update;
		if (isGradle) {
			version = properties.getProperty("version");
		} else {
			version = properties.getProperty("rapidminer.version", "0");
			revision = properties.getProperty("rapidminer.revision", "0");
			update = properties.getProperty("rapidminer.update", "000");
			version += "." + revision + "." + update;
		}
		if (version.compareTo(RM_MIN_VERSION) < 0) {
			return false;
		}
		if (version.compareTo(rmVersion) < 0) {
			return false;
		}
		rmVersion = version;
		rapidMiner = proj;
		rmIsGradle = isGradle;
		System.out.println("RM: " + proj + ", version: " + version);
		return true;
	}

	/**
	 * Returns whether the extension is active, e.g. there is a RapidMiner
	 * project.
	 *
	 * @return <tt>true</tt> if the extension is active.
	 */
	public boolean isActive() {
		return rapidMiner != null /* && !antBuildFiles.isEmpty() */;
	}

	/**
	 * Returns the set of changed extension projects.
	 *
	 * @return the set of changed extensions
	 */
	public Set<IProject> getChangedProjects() {
		return projListener.getChangedProjects();
	}

	/**
	 * Readds failed projects to the change list to be processed again at the
	 * next build.
	 *
	 * @param failedProjects
	 *            The set of failed builds.
	 * @see RMProjectListener#addChangedProjects(Set)
	 */
	public void addChangedProjects(Set<IProject> failedProjects) {
		failedProjects.retainAll(getProjects());
		projListener.addChangedProjects(failedProjects);
	}

	/**
	 * Calls the {@link RMProjectListener#waitForChanges} method to wait for
	 * changes in projects to be processed.
	 */
	public void waitForChanges() {
		projListener.waitForChanges();
	}

}
