/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2013-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	http://www-ai.cs.uni-dortmund.de/SOFTWARE/RMD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer;

import ide2rapidminer.interfaces.Constants;
import ide2rapidminer.interfaces.OpenAndActivate;
import ide2rapidminer.interfaces.ReloadOperators;
import ide2rapidminer.local.ReplaceAction;
import ide2rapidminer.local.ReplaceAction.Status;
import ide2rapidminer.remote.EditSourceCodeAction;
import ide2rapidminer.remote.ReplacePlugins;
import ide2rapidminer.util.Compatibility;

import java.awt.BorderLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import com.rapidminer.gui.GeneralProcessListener;
import com.rapidminer.gui.MainFrame;
import com.rapidminer.gui.RapidMinerGUI;
import com.rapidminer.gui.docking.RapidDockingToolbar;
import com.rapidminer.gui.flow.ProcessInteractionListener;
import com.rapidminer.gui.tools.ResourceAction;
import com.rapidminer.gui.tools.SwingTools;
import com.rapidminer.gui.tools.VersionNumber;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorChain;
import com.rapidminer.operator.ports.Port;
import com.rapidminer.tools.FileSystemService;
import com.rapidminer.tools.LogService;
import com.rapidminer.tools.plugin.Plugin;
import com.vlsolutions.swing.toolbars.ToolBarConstraints;
import com.vlsolutions.swing.toolbars.ToolBarContainer;
import com.vlsolutions.swing.toolbars.ToolBarPanel;

/**
 * The class for RapidMiner to initialize this extension. The methods are called
 * during initializing through RapidMiner to configure the extension's
 * contributions to RapidMiner
 *
 * @author Jan Czogalla
 */
public class PluginInitIDE2R {

	/**
	 * The namespace of this extension. Used for getting GUI related strings.
	 */
	public final static String KEY = "ide2rm";
	/**
	 * Property name for the last file used to replace extensions.
	 */
	public final static String LAST_FILE = "last.file";
	/**
	 * Property name for the last extension that was replaced.
	 */
	public final static String LAST_PLUGIN = "last.plugin";
	/**
	 * Property name for the last setting of the "load temporarily" option.
	 */
	public final static String LAST_TEMP = "last.temp";
	/**
	 * Property name for the last setting of the "use older version" option.
	 */
	public final static String LAST_OLD = "last.old";
	/**
	 * Property name for the last setting of the "check on process start"
	 * option.
	 */
	public final static String LAST_START = "last.start";
	/**
	 * Property name for the last setting of the "confirm loading" option.
	 */
	public final static String CONFIRM_REPLACE = "confirm";
	/**
	 * Property name for the last setting of the "show success message" option.
	 */
	public final static String SHOW_RESULT = "show.result";

	// GUI related fields that are initialized at extension start up, and are
	// needed to tear down the extension.
	private static Registry reg;
	private static ReplaceAction replaceAction;
	private static JMenuItem raComp;
	private static ReloadOperators ideListener;
	private static ResourceAction editSourceAction;
	private static JMenuItem esaComp;
	private static ProcessInteractionListener popupListener;
	private static RapidDockingToolbar toolbar;
	private static ToolBarPanel tbPanel;
	private static GeneralProcessListener updateListener;

	// property and compatibility fields
	private static File propFile;
	private static Properties properties;
	private static boolean incompatible = false;
	private static Compatibility compatibility = Compatibility.getDefault();

	/**
	 * This method will be called directly after the extension is initialized.
	 * This is the first hook during start up. No initialization of the
	 * operators or renderers has taken place when this is called.
	 */
	public static void initPlugin() {
		// check if extension is compatible, i.e. if the reregister method
		// exists
		VersionNumber current = RapidMiner.getVersion();
		VersionNumber five = new VersionNumber(5, 3);
		VersionNumber six = new VersionNumber(6, 0, 2);
		// Versions below 5.3 are not tested
		if (current.compareTo(five) < 0) {
			LogService.getRoot().warning("Incompatible RapidMiner version.");
			incompatible = true;
			return;
		} else if (current.compareTo(six) < 0) {
			// previous to 6.0.2, the patch needs to be applied
			try {
				Plugin.class.getMethod("reregister");
			} catch (NoSuchMethodException nsme) {
				LogService.getRoot()
						.warning("Incompatible RapidMiner version.");
				incompatible = true;
				return;
			}
		}

		properties = createProperties();
		RapidMinerGUI.addShutdownHook(new Runnable() {

			@Override
			public void run() {
				saveProperties();
			}
		});
		// tear down all extensions at shutdown, so marked files can be deleted
		RapidMinerGUI.addShutdownHook(new Runnable() {

			@Override
			public void run() {
				for (Plugin pl : new ArrayList<Plugin>(Plugin.getAllPlugins())) {
					pl.tearDown();
				}
			}
		});
		try {
			reg = LocateRegistry.createRegistry(Constants.RMI_PORT_IDE2RM);
		} catch (RemoteException e) {
			reg = null;
		}
	}

	/**
	 * Creates the properties for this extension either by creating a new set of
	 * properties with the default values and completing them by reading from
	 * the associated user configuration file.
	 *
	 * @return the loaded or newly created properties.
	 */
	private static Properties createProperties() {
		propFile = FileSystemService.getUserConfigFile(KEY);
		Properties properties = new Properties();
		// put defaults, then check for file content
		properties.put(LAST_FILE, "");
		properties.put(LAST_PLUGIN, "");
		properties.put(LAST_TEMP, "true");
		properties.put(LAST_OLD, "false");
		properties.put(LAST_START, "false");
		properties.put(CONFIRM_REPLACE, "true");
		properties.put(SHOW_RESULT, "true");
		if (propFile != null && propFile.length() > 0) {
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(propFile);
				properties.load(fis);
			} catch (IOException e) {
			} finally {
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException e) {
					}
				}
			}
		}
		return properties;
	}

	/**
	 * Returns whether the extension is active or not, i.e. if it is connected
	 * to an IDE via RMI.
	 *
	 * @return <code>true</code> if the extension is active.
	 */
	public static boolean isActive() {
		return reg != null;
	}

	/**
	 * Returns whether the extension is compatible with the RapidMiner version
	 * or not.
	 *
	 * @return <code>true</true> if the extension is compatible.
	 */
	public static boolean isCompatible() {
		return !incompatible;
	}

	/**
	 * This method is called during start up as the second hook. It is called
	 * before the gui of the mainframe is created. The Mainframe is given to
	 * adapt the gui. The operators and renderers have been registered in the
	 * meanwhile.
	 */
	public static void initGui(final MainFrame mainFrame) {
		if (incompatible) {
			return;
		}

		compatibility.setMainFrame(mainFrame);

		// TODO?
		// addReplaceAction(mainFrame);

		if (!isActive()) {
			return;
		}
		// create remote reload object
		ideListener = new ReplacePlugins(mainFrame);
		try {
			ReloadOperators rlOp = (ReloadOperators) UnicastRemoteObject
					.exportObject(ideListener, 0);
			reg.rebind(ReloadOperators.RELOADER, rlOp);
		} catch (RemoteException e) {
			e.printStackTrace();
			destroyRMI();
			return;
		}
		// test if remote open object exists from IDE
		try {
			LocateRegistry.getRegistry(Constants.RMI_PORT_RM2E).lookup(
					OpenAndActivate.OPENER);
		} catch (Exception e) {
			e.printStackTrace();
			destroyRMI();
			return;
		}
		// adding the "edit source code" action
		JMenu edit = mainFrame.getEditMenu();

		int position = getPosition(edit, "rename_in_processrenderer");
		if (position == -1) {
			destroyRMI();
			return;
		}
		editSourceAction = new EditSourceCodeAction(mainFrame);
		editSourceAction.addToActionMap(JComponent.WHEN_FOCUSED, true, true,
				null, mainFrame.getProcessPanel().getProcessRenderer(),
				mainFrame.getOperatorTree());
		esaComp = edit.add(editSourceAction);
		edit.remove(esaComp);
		edit.add(esaComp, position);

		// adding "edit source code" action to context menu
		popupListener = new ProcessInteractionListener() {

			@Override
			public void portContextMenuWillOpen(JPopupMenu menu, Port port) {
				// Don't care.
			}

			@Override
			public void operatorContextMenuWillOpen(JPopupMenu menu,
					Operator operator) {
				// insert edit action in popup menu
				int pos = getPosition(menu, "rename_in_processrenderer");
				if (pos == -1) {
					return;
				}
				JMenuItem esaComp = menu.add(editSourceAction);
				menu.remove(esaComp);
				menu.add(esaComp, pos);
			}

			@Override
			public void operatorMoved(Operator op) {
				// Don't care.
			}

			@Override
			public void displayedChainChanged(OperatorChain displayedChain) {
				// Don't care.
			}
		};
		compatibility.addProcessInteractionListener(popupListener);
	}

	/**
	 * Gets the exact position of a specified action in the given component. The
	 * component has to be either a JMenu or JPopupMenu. Will return the
	 * position of the action or -1 if it does not exist.
	 *
	 * @param comp
	 *            - the component to search in.
	 * @param key
	 *            - the key of the action
	 * @return The exact position of the specified action.
	 */
	private static int getPosition(JComponent comp, String key) {
		// get the popup menu
		JPopupMenu menu;
		if (comp instanceof JPopupMenu) {
			menu = (JPopupMenu) comp;
		} else if (comp instanceof JMenu) {
			menu = ((JMenu) comp).getPopupMenu();
		} else {
			return -1;
		}
		int entries = menu.getComponentCount();
		JMenuItem item;
		ResourceAction resAct;
		Object tmp;
		// search through entries for key
		for (int i = 0; i < entries; i++) {
			tmp = menu.getComponent(i);
			if (!(tmp instanceof JMenuItem)) {
				continue;
			}
			item = (JMenuItem) tmp;
			resAct = (ResourceAction) item.getAction();
			if (key.equalsIgnoreCase(resAct.getKey())) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Destroys the RMI registry and objects if necessary
	 */
	private static void destroyRMI() {
		if (reg == null) {
			return;
		}
		if (ideListener != null) {
			try {
				UnicastRemoteObject.unexportObject(ideListener, true);
			} catch (NoSuchObjectException e) {
				e.printStackTrace();
			}
		}
		try {
			UnicastRemoteObject.unexportObject(reg, true);
		} catch (NoSuchObjectException e) {
			e.printStackTrace();
		}
		reg = null;
		ideListener = null;
	}

	/**
	 * The last hook before the splash screen is closed. Third in the row.
	 */
	public static void initFinalChecks() {
		// TODO dont restart right away
		updateListener = new GeneralProcessListener() {

			@Override
			public void processStarts(Process process) {
				boolean checkOnStart = Boolean.valueOf(properties.getProperty(
						LAST_START, "false"));
				if (!checkOnStart) {
					return;
				}
				String fileName = properties.getProperty(LAST_FILE, "");
				if (ReplaceAction.isURL(fileName)) {
					return;
				}
				boolean temp = Boolean.valueOf(properties.getProperty(
						LAST_TEMP, "true"));
				boolean older = Boolean.valueOf(properties.getProperty(
						LAST_OLD, "false"));
				process.pause();
				Status result = replaceAction.checkFile(null, null, fileName,
						temp, older);
				if (result != Status.SUCCESS) {
					process.resume();
					return;
				}
				process.stop();
				// TODO do anything if negative result?
				replaceAction.replace();
				RapidMinerGUI.getMainFrame().RUN_ACTION.actionPerformed(null);
			}

			@Override
			public void processStartedOperator(Process process, Operator op) {
				// don't care
			}

			@Override
			public void processFinishedOperator(Process process, Operator op) {
				// don't care
			}

			@Override
			public void processEnded(Process process) {
				// don't care
			}
		};
		if (replaceAction == null) {
			addReplaceAction(RapidMinerGUI.getMainFrame());
		}
	}

	/**
	 * Adds the replace action to both the extension menu and the toolbar. Will
	 * be called two times, in {@link #initFinalChecks()} and
	 * {@link #initGui(MainFrame)}, depending on whether the menus or menu items
	 * already exist.
	 *
	 * @param mainFrame
	 *            The RapidMiner mainframe.
	 */
	private static void addReplaceAction(final MainFrame mainFrame) {
		if (mainFrame == null) {
			return;
		}
		// adding the "load/replace extension" action
		JMenu extMenu = getExtensionsMenu(mainFrame);
		int replacePosition = getPosition(extMenu, "manage_extensions");
		// TODO
		// if (replacePosition == -1) {
		// return;
		// }
		replaceAction = new ReplaceAction(mainFrame, properties);
		raComp = extMenu.add(replaceAction);
		if (replacePosition != -1) {
			extMenu.remove(raComp);
			extMenu.add(raComp, replacePosition);
		}
		// adding load/replace to toolbar
		toolbar = new RapidDockingToolbar(KEY);
		JButton replaceButton = new JButton(replaceAction);
		replaceButton.setText("");
		toolbar.add(replaceButton);
		tbPanel = ((ToolBarContainer) mainFrame.getContentPane()
				.getComponent(0)).getToolBarPanelAt(BorderLayout.NORTH);
		int tbCount = tbPanel.getComponentCount();
		tbPanel.add(toolbar, new ToolBarConstraints(0, tbCount));
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				mainFrame.validate();
				mainFrame.repaint();
			}
		});
	}

	/**
	 * Returns the menu containing the items associated with extensions. Since
	 * RapidMiner 7.0, there is a specific extension menu, before that the items
	 * were located in the help menu.
	 *
	 * @param mainFrame
	 *            The RapidMiner mainframe.
	 * @return Either the extension menu (RM 7.0 and up) or the help menu.
	 */
	public static JMenu getExtensionsMenu(MainFrame mainFrame) {
		return compatibility.getExtensionsMenu();
	}

	/**
	 * Will be called as fourth method, directly before the UpdateManager is
	 * used for checking updates. Location for exchanging the UpdateManager. The
	 * name of this method unfortunately is a result of a historical typo, so
	 * it's a little bit misleading.
	 */
	public static void initPluginManager() {
		if (incompatible) {
			// show error if the extension could not be loaded due to
			// incompatibility
			SwingTools.showVerySimpleErrorMessage(KEY + ".startup_failed");
		}
	}

	/**
	 * Will be called when the plugin should be removed. This can happen on
	 * reloading for changes in the plugin or if the plugin should be disabled.
	 * In this method all additions to the GUI created by this plugin should be
	 * removed.
	 */
	public static void tearDownGUI(final MainFrame mainFrame) {
		if (incompatible) {
			return;
		}
		if (raComp != null) {
			mainFrame.getHelpMenu().remove(raComp);
		}
		if (esaComp != null) {
			mainFrame.getEditMenu().remove(esaComp);
		}
		if (popupListener != null) {
			compatibility.removeProcessInteractionListener(popupListener);
		}
		if (tbPanel != null) {
			toolbar.removeAll();
			tbPanel.remove(toolbar);
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					mainFrame.validate();
					mainFrame.repaint();
				}
			});
		}
		if (updateListener != null) {
			mainFrame.getProcess().getRootOperator()
					.removeProcessListener(updateListener);
		}
	}

	/**
	 * Will be called after tearDownGUI and should remove all non-GUI related
	 * additions this plugin made during initialization.
	 */
	public static void tearDown() {
		if (incompatible) {
			return;
		}
		saveProperties();
		destroyRMI();
	}

	/**
	 * Saves the properties to the user configuration file.
	 */
	public static void saveProperties() {
		if (propFile == null) {
			return;
		}
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(propFile);
			properties.store(fos, "");
		} catch (IOException e) {
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
				}
			}
		}
	}
}
