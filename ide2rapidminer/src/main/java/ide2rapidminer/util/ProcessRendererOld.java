/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2013-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	http://www-ai.cs.uni-dortmund.de/SOFTWARE/RMD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package ide2rapidminer.util;

import ide2rapidminer.util.interfaces.ProcessRendererProxy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.rapidminer.gui.MainFrame;
import com.rapidminer.gui.flow.ProcessInteractionListener;
import com.rapidminer.gui.flow.ProcessPanel;
import com.rapidminer.gui.flow.processrendering.model.ProcessRendererModel;
import com.rapidminer.gui.flow.processrendering.view.ProcessRendererView;
import com.rapidminer.operator.OperatorChain;

/**
 * The process renderer proxy for RapidMiner 5.3 to 6.5. Uses the old
 * ProcessRenderer class that is split into {@link ProcessRendererView} and
 * {@link ProcessRendererModel} since RapidMiner 6.5. Reflection is used to
 * retrieve the old and now nonexistent classes and methods.
 *
 * @author Jan Czogalla
 *
 */
public class ProcessRendererOld implements ProcessRendererProxy {

	/**
	 * The ProcessRenderer instance associated with the running RapidMiner
	 * {@link MainFrame}
	 */
	private Object processRenderer;
	/**
	 * The {@link ProcessPanel} instance associated with the running RapidMiner
	 * {@link MainFrame}
	 */
	private ProcessPanel processPanel;

	/**
	 * The method to retrieve the {@link #processRenderer ProcessRenderer}
	 */
	private static Method getRenderer;
	/**
	 * The method to add a {@link ProcessInteractionListener} from the
	 * {@link #processRenderer ProcessRenderer} class
	 */
	private static Method addProcList;
	/**
	 * The method to remove a {@link ProcessInteractionListener} from the
	 * {@link #processRenderer ProcessRenderer} class
	 */
	private static Method remProcList;
	/**
	 * The method to retrieve the currently displayed {@link OperatorChain} from
	 * the {@link #processRenderer ProcessRenderer} class
	 */
	private static Method getDispChain;

	static {
		try {
			getRenderer = ProcessPanel.class
					.getDeclaredMethod("getProcessRenderer");
			Class<?> processRendererClass = getRenderer.getReturnType();
			addProcList = processRendererClass.getDeclaredMethod(
					"addProcessInteractionListener",
					ProcessInteractionListener.class);
			remProcList = processRendererClass.getDeclaredMethod(
					"removeProcessInteractionListener",
					ProcessInteractionListener.class);
			getDispChain = processRendererClass
					.getDeclaredMethod("getDisplayedChain");
		} catch (NoSuchMethodException e) {
			// should not happen, since it is only called in old RapidMiner
			// instances
			e.printStackTrace();
		}

	}

	/**
	 * Retrieves the {@link ProcessPanel} and {@link #processRenderer
	 * ProcessRenderer} from the {@link MainFrame}.
	 */
	@Override
	public void setMainFrame(MainFrame mainframe) {
		processPanel = mainframe.getProcessPanel();
		try {
			processRenderer = getRenderer.invoke(processPanel);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addProcessInteractionListener(ProcessInteractionListener pil) {
		try {
			addProcList.invoke(processRenderer, pil);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void removeProcessInteractionListener(ProcessInteractionListener pil) {
		try {
			remProcList.invoke(processRenderer, pil);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	@Override
	public OperatorChain getDisplayedChain() {
		try {
			return (OperatorChain) getDispChain.invoke(getRenderer);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Uses the deprecated {@link ProcessPanel#showOperatorChain(OperatorChain)}
	 * method to set the currently displayed {@link OperatorChain}.
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void setDisplayedChain(OperatorChain opChain) {
		processPanel.showOperatorChain(opChain);
	}

}
