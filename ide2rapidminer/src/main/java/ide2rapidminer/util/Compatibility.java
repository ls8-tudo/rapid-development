/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2013-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	http://www-ai.cs.uni-dortmund.de/SOFTWARE/RMD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package ide2rapidminer.util;

import ide2rapidminer.util.interfaces.ExtensionMenuProxy;
import ide2rapidminer.util.interfaces.ProcessRendererProxy;

import javax.swing.JMenu;

import com.rapidminer.gui.MainFrame;
import com.rapidminer.gui.flow.ProcessInteractionListener;
import com.rapidminer.operator.OperatorChain;

/**
 * A collection class that implements all compatibility interfaces and manages
 * the correct implementations for the currently running RapidMiner.
 *
 * @author Jan Czogalla
 *
 */
public class Compatibility implements ProcessRendererProxy, ExtensionMenuProxy {

	/**
	 * The process renderer proxy.
	 */
	private static ProcessRendererProxy procRenProxy;
	static {
		// Since 6.5, the process renderer has been split up into a view and
		// model
		try {
			Class.forName("com.rapidminer.gui.flow.processrendering.view.ProcessRendererView");
			procRenProxy = new ProcessRendererSixFive();
		} catch (ClassNotFoundException cnfe) {
			procRenProxy = new ProcessRendererOld();
		}
	}

	/**
	 * The extension menu proxy.
	 */
	private static ExtensionMenuProxy extMenProxy;
	static {
		// Since 7.0, there is an explicit extensions menu, and no longer a help
		// menu. In 6.5, things still go over the help menu
		try {
			MainFrame.class.getMethod("getExtensionsMenu");
			extMenProxy = new ExtensionMenuSeven();
		} catch (NoSuchMethodException nsme) {
			extMenProxy = new ExtensionMenuOld();
		}
	}

	/**
	 * The singleton instance.
	 */
	private static Compatibility instance = new Compatibility();

	// private constructor since this is a singleton
	private Compatibility() {
	}

	/**
	 * Returns the singleton instance.
	 *
	 * @return the singleton instance.
	 */
	public static Compatibility getDefault() {
		return instance;
	}

	/**
	 * Sets the {@link MainFrame} for all proxies.
	 */
	@Override
	public void setMainFrame(MainFrame mainframe) {
		procRenProxy.setMainFrame(mainframe);
		extMenProxy.setMainFrame(mainframe);
	}

	@Override
	public void addProcessInteractionListener(ProcessInteractionListener pil) {
		procRenProxy.addProcessInteractionListener(pil);
	}

	@Override
	public void removeProcessInteractionListener(ProcessInteractionListener pil) {
		procRenProxy.removeProcessInteractionListener(pil);
	}

	@Override
	public OperatorChain getDisplayedChain() {
		return procRenProxy.getDisplayedChain();
	}

	@Override
	public void setDisplayedChain(OperatorChain opChain) {
		procRenProxy.setDisplayedChain(opChain);
	}

	@Override
	public JMenu getExtensionsMenu() {
		return extMenProxy.getExtensionsMenu();
	}

}
