/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2013-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	http://www-ai.cs.uni-dortmund.de/SOFTWARE/RMD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package ide2rapidminer.util;

import ide2rapidminer.util.interfaces.ExtensionMenuProxy;

import javax.swing.JMenu;

import com.rapidminer.gui.MainFrame;

/**
 * The extension menu proxy for RapidMiner 5.3 to 7.0. Uses the old
 * {@link MainFrame#getHelpMenu()} method. Since RapidMiner 7.0 there is a new
 * extensions menu.
 *
 * @author Jan Czogalla
 *
 */
public class ExtensionMenuOld implements ExtensionMenuProxy {

	/**
	 * The {@link MainFrame} instance associated with the running RapidMiner
	 */
	private MainFrame mainframe;

	@Override
	public void setMainFrame(MainFrame mainframe) {
		this.mainframe = mainframe;
	}

	@Override
	public JMenu getExtensionsMenu() {
		return mainframe.getHelpMenu();
	}

}
