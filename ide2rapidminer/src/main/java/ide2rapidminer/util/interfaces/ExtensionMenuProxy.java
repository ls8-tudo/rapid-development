/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2013-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	http://www-ai.cs.uni-dortmund.de/SOFTWARE/RMD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package ide2rapidminer.util.interfaces;

import javax.swing.JMenu;

/**
 * Proxy interface for retrieving the extensions menu in different RapidMiner
 * versions.
 *
 * @author Jan Czogalla
 *
 */
public interface ExtensionMenuProxy extends MainFrameProxy {

	/**
	 * Returns the menu handling extensions in RapidMiner.
	 *
	 * @return the extension menu.
	 */
	public JMenu getExtensionsMenu();

}
