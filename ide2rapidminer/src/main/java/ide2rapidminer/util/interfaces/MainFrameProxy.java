package ide2rapidminer.util.interfaces;

import com.rapidminer.gui.MainFrame;

/**
 * Super interface for compatibility proxies regarding the RapidMiner
 * {@link MainFrame}.
 *
 * @author Jan Czogalla
 *
 */
public interface MainFrameProxy {

	/**
	 * Sets the {@link MainFrame} for this proxy.
	 *
	 * @param mainframe
	 *            The RapidMiner {@link MainFrame}
	 */
	public void setMainFrame(MainFrame mainframe);

}
