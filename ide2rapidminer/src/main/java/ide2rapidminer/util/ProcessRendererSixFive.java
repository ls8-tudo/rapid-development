/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2013-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	http://www-ai.cs.uni-dortmund.de/SOFTWARE/RMD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package ide2rapidminer.util;

import ide2rapidminer.util.interfaces.ProcessRendererProxy;

import com.rapidminer.gui.MainFrame;
import com.rapidminer.gui.flow.ProcessInteractionListener;
import com.rapidminer.gui.flow.ProcessPanel;
import com.rapidminer.gui.flow.processrendering.model.ProcessRendererModel;
import com.rapidminer.gui.flow.processrendering.view.ProcessRendererView;
import com.rapidminer.operator.OperatorChain;

/**
 * The process renderer proxy for RapidMiner 6.5 and up. Uses the new
 * {@link ProcessRendererView} and {@link ProcessRendererModel} classes to
 * access {@link ProcessInteractionListener ProcessInteractionListeners} and the
 * currently displayed {@link OperatorChain}.
 *
 * @author Jan Czogalla
 *
 */
public class ProcessRendererSixFive implements ProcessRendererProxy {

	/**
	 * The {@link ProcessRendererView} instance associated with the running
	 * RapidMiner {@link MainFrame}
	 */
	private ProcessRendererView prv;

	/**
	 * Retrieves the {@link ProcessRendererView} from the {@link ProcessPanel}
	 * via the {@link MainFrame}.
	 */
	@Override
	public void setMainFrame(MainFrame mainframe) {
		prv = mainframe.getProcessPanel().getProcessRenderer();
	}

	@Override
	public void addProcessInteractionListener(ProcessInteractionListener pil) {
		prv.addProcessInteractionListener(pil);
	}

	@Override
	public void removeProcessInteractionListener(ProcessInteractionListener pil) {
		prv.removeProcessInteractionListener(pil);
	}

	@Override
	public OperatorChain getDisplayedChain() {
		return prv.getModel().getDisplayedChain();
	}

	@Override
	public void setDisplayedChain(OperatorChain opChain) {
		prv.getModel().setDisplayedChain(opChain);
	}

}
