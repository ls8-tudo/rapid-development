/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2013-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	http://www-ai.cs.uni-dortmund.de/SOFTWARE/RMD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package ide2rapidminer.local;

import static com.rapidminer.PluginInitIDE2R.CONFIRM_REPLACE;
import static com.rapidminer.PluginInitIDE2R.KEY;
import static com.rapidminer.PluginInitIDE2R.LAST_FILE;
import static com.rapidminer.PluginInitIDE2R.LAST_OLD;
import static com.rapidminer.PluginInitIDE2R.LAST_PLUGIN;
import static com.rapidminer.PluginInitIDE2R.LAST_START;
import static com.rapidminer.PluginInitIDE2R.LAST_TEMP;
import static com.rapidminer.PluginInitIDE2R.SHOW_RESULT;
import ide2rapidminer.remote.ReplacePlugins;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.io.FileUtils;

import com.rapidminer.PluginInitIDE2R;
import com.rapidminer.gui.MainFrame;
import com.rapidminer.gui.RapidMinerGUI;
import com.rapidminer.gui.tools.ProgressThread;
import com.rapidminer.gui.tools.ResourceAction;
import com.rapidminer.gui.tools.SwingTools;
import com.rapidminer.gui.tools.components.FixedWidthLabel;
import com.rapidminer.gui.tools.dialogs.ButtonDialog;
import com.rapidminer.gui.tools.dialogs.ConfirmDialog;
import com.rapidminer.tools.I18N;
import com.rapidminer.tools.ProgressListener;
import com.rapidminer.tools.plugin.Plugin;

/**
 * The action class to replace an existing extension or load and register a new
 * one.
 *
 * @author Jan Czogalla
 *
 */
public class ReplaceAction extends ResourceAction {

	private static final long serialVersionUID = 518016512143719466L;

	/**
	 * Enum of return statuses for {@link ReplaceAction#replace() replace} and
	 * {@link ReplaceAction#checkFile(ReplacePluginDialog, Plugin, String, boolean, boolean)
	 * checkFile}.
	 */
	public enum Status {
		FAIL, SUCCESS, NOT_EXIST, IO_ERROR, NOT_COMPATIBLE, NOT_DEPLOYABLE, OLD_VERSION, ABORT, NO_CHANGE
	};

	/**
	 * File that is used as a return value for
	 * {@link ReplaceAction#createFromURL(String, ReplacePluginDialog, boolean)
	 * createFromURL} to denote an IO error.
	 */
	public static final File IO_ERROR_FILE = new File("/IO_ERROR");

	/**
	 * Prefix for a new loaded extension since it has no extension namespace to
	 * replace.
	 */
	public static final String NO_REPLACE_PREFIX = "none";
	/**
	 * Prefix for temporary downloaded extension files.
	 */
	public static final String TMP_URL_PREFIX = "extensionFromURL";
	/**
	 * Suffix for temporary downloaded extension files.
	 */
	public static final String TMP_URL_SUFFIX = ".jar";

	// properties and replace worker
	private ReplacePlugins ideListener;
	private Properties properties;

	// fields to store the progress of the replace process
	private Plugin oldPlugin;
	private File newPluginFile;
	private boolean isOlder;
	private boolean isTemp = true;
	private boolean isURL;

	// collection of temporary downloaded files
	private static Map<String, File> cachedURLs = new HashMap<String, File>();
	static {
		// remove temporary URL files from last run if there are any left
		File tmpDir = new File(System.getProperty("java.io.tmpdir"));
		try {
			for (File tmp : tmpDir.listFiles(new FilenameFilter() {

				@Override
				public boolean accept(File dir, String name) {
					return name.startsWith(TMP_URL_PREFIX)
							&& name.endsWith(TMP_URL_SUFFIX);
				}
			})) {
				tmp.delete();
			}
		} catch (Exception e) {
		}
	}

	// random object for newly loaded extensions
	private static Random rand = new Random();

	// mapping of files and last modified
	private static Map<String, Long> lastModified = new HashMap<String, Long>();

	// fields for monitoring/ executing the download process
	private static ProgressThread pt;
	private static SwingWorker<Void, Void> downloader;

	/**
	 * Default constructor for this action.
	 *
	 * @param mainFrame
	 *            - the RapidMiner MainFrame.
	 * @param properties
	 *            - the properties for this action.
	 */
	public ReplaceAction(MainFrame mainFrame, Properties properties) {
		super(KEY + ".replace");
		setCondition(PROCESS_RUNNING, DISALLOWED);
		this.properties = properties;
		this.ideListener = new ReplacePlugins(mainFrame);
	}

	/**
	 * Invokes the extension replace dialog.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		ReplacePluginDialog rpd = new ReplacePluginDialog(this, properties);
		rpd.setResizable(false);
		rpd.setVisible(true);
	}

	/**
	 * Checks a given file and extension for compatibility.
	 *
	 * @param rpd
	 *            - The {@link ReplacePluginDialog} associated with the current
	 *            loading process or null if invoked programmatically.
	 * @param plugin
	 *            - The extension to be matched to the newly loaded extension or
	 *            null if it is not known which extension to replace.
	 * @param fileName
	 *            - The file name containing the new extension.
	 * @param temp
	 *            - Denotes if the loading will be temporarily.
	 * @param older
	 *            - Denotes if older versions are allowed.
	 * @return The status of the operation, one of {@link Status}.
	 */
	public Status checkFile(ReplacePluginDialog rpd, Plugin plugin,
			String fileName, boolean temp, boolean older) {
		if (!fileChanged(fileName)) {
			return Status.NO_CHANGE;
		}
		// isURL = isURL(fileName);
		File newFile;
		if (isURL) {
			newFile = createFromURL(fileName, rpd, temp);
			if (newFile == IO_ERROR_FILE) {
				return Status.IO_ERROR;
			}
			if (newFile == null) {
				return Status.ABORT;
			}
		} else {
			newFile = new File(fileName);
		}
		if (newFile == null || !newFile.exists()) {
			return Status.NOT_EXIST;
		}
		Plugin newPlugin;
		try {
			newPlugin = new Plugin(newFile);
		} catch (IOException ioe) {
			if (rpd != null) {
				rpd.setException(ioe);
			}
			return Status.IO_ERROR;
		}
		String newID = newPlugin.getExtensionId();
		String newVersion = newPlugin.getVersion();
		if (newID == null || newVersion == null) {
			return Status.NOT_COMPATIBLE;
		}
		if (newID.endsWith(KEY)) {
			return Status.NOT_DEPLOYABLE;
		}
		if (plugin == null) {
			plugin = Plugin.getPluginByExtensionId(newID);
		}
		if (plugin != null && !newID.equals(plugin.getExtensionId())) {
			return Status.NOT_COMPATIBLE;
		}
		if (plugin != null && plugin.getVersion().compareTo(newVersion) > 0) {
			if (!older) {
				return Status.OLD_VERSION;
			} else {
				isOlder = true;
			}
		} else {
			isOlder = false;
		}
		if (plugin != null) {
			oldPlugin = plugin;
			if (rpd != null) {
				rpd.setPlugin(plugin);
			}
		}
		isTemp = temp;
		newPluginFile = newFile;
		return Status.SUCCESS;
	}

	/**
	 * Checks whether the given file name or URL did change since the last time
	 * they were requested. This will only return false if the file/URL was seen
	 * before and is still valid.
	 *
	 * @param fileName
	 *            - The file name or URL to be checked.
	 * @return false if it didn't change and is still valid or true otherwise.
	 */
	private boolean fileChanged(String fileName) {
		isURL = isURL(fileName);
		long oldMod = -1;
		if (lastModified.containsKey(fileName)) {
			oldMod = lastModified.get(fileName);
		}
		long newMod;
		if (isURL) {
			try {
				URL url = new URL(fileName);
				URLConnection connection = url.openConnection();
				connection.connect();
				newMod = connection.getLastModified();
			} catch (Exception e) {
				return true;
			}
			if (cachedURLs.containsKey(fileName)
					&& !cachedURLs.get(fileName).exists()) {
				return true;
			}
		} else {
			File file = new File(fileName);
			newMod = file.lastModified();
		}
		if (newMod != 0) {
			lastModified.put(fileName, newMod);
		}
		return oldMod != newMod;
	}

	/**
	 * Checks whether the last requested file or URL has changed.
	 *
	 * @return false if the given file or URL did not change and is still valid.
	 * @see #fileChanged(String)
	 */
	public boolean fileChanged() {
		return fileChanged(properties.getProperty(LAST_FILE, ""));
	}

	/**
	 * Resets the stored values of the
	 * {@link #checkFile(ReplacePluginDialog, Plugin, String, boolean, boolean)
	 * checkFile} method and stops the download if necessary.
	 */
	public void reset() {
		if (isURL) {
			pt.cancel();
		}
		oldPlugin = null;
		newPluginFile = null;
		isOlder = isURL = false;
		isTemp = true;
	}

	/**
	 * The actual replacing/loading process. Uses the values determined in
	 * {@link #checkFile(ReplacePluginDialog, Plugin, String, boolean, boolean)
	 * checkFile} to load the new extension.
	 *
	 * @return The status of the operation, one of {@link Status#FAIL FAIL} and
	 *         {@link Status#SUCCESS SUCCESS}
	 */
	public Status replace() {
		if (newPluginFile == null) {
			reset();
			return Status.FAIL;
		}
		Map<String, String> replaceMap = new HashMap<String, String>();
		String oldID = oldPlugin != null ? oldPlugin.getExtensionId()
				: (NO_REPLACE_PREFIX + rand.nextInt());
		if (!isTemp) {
			if (isURL) {
				// copy temporary downloaded extension files
				File newFile = getPluginFileWithName(newPluginFile);
				if (newFile != null) {
					try {
						FileUtils.moveFile(newPluginFile, newFile);
						updateCache(newPluginFile, newFile);
						newPluginFile = newFile;
					} catch (IOException e) {
					}
				}
			}
			if (isOlder && !oldPlugin.getFile().equals(newPluginFile)) {
				oldPlugin.getFile().deleteOnExit();
			}
			ideListener.markForCopy(oldID);
		} else if (isURL) {
			newPluginFile.deleteOnExit();
		}
		replaceMap.put(oldID, newPluginFile.getAbsolutePath());
		if (!ideListener.reload(replaceMap)) {
			reset();
			return Status.FAIL;
		}
		// ideListener.toFront();
		return Status.SUCCESS;
	}

	/**
	 * Creates a new file from a temporary extension file. The new file will
	 * match the standard RapidMiner extension file pattern.
	 *
	 * @param file
	 *            - The temporary file.
	 * @return The new file or null if the specified file is not an extension.
	 */
	private static File getPluginFileWithName(File file) {
		try {
			Plugin plugin = new Plugin(file);
			return new File(file.getParent(), plugin.getName() + "-"
					+ plugin.getVersion() + ".jar");
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * Determines the current directory based on the specified filename.
	 *
	 * @param fileName
	 *            - The filename used as a basis.
	 * @return The file specified by the filename, the first of its existent
	 *         parents or null if the filename is an URL.
	 */
	private static File createCurrentDirectory(String fileName) {
		if (fileName == null || fileName.trim().isEmpty()) {
			return null;
		}
		fileName = fileName.trim();
		if (isURL(fileName)) {
			return null;
		}
		File result = new File(fileName);
		while (!result.exists() && (result = result.getParentFile()) != null) {
		}
		return result;
	}

	/**
	 * Checks if the given filename is an URL.
	 *
	 * @param fileName
	 *            -The filename to be checked.
	 * @return If the filename is an URL.
	 */
	public static boolean isURL(String fileName) {
		try {
			new URL(fileName);
		} catch (MalformedURLException murle) {
			return false;
		}
		return true;
	}

	/**
	 * Creates a file from the given URL. Will start a download or get the file
	 * associated with the URL from the local cache.
	 *
	 * @param fileName
	 *            - The URL to convert to a file.
	 * @param rpd
	 *            - The {@link ReplacePluginDialog} associated with the current
	 *            loading process or null if invoked programmatically.
	 * @param temp
	 *            - Denotes if the loading will be temporarily.
	 * @return The file associated with the URL, null if the download was
	 *         stopped or {@link #IO_ERROR_FILE} if an IO error occured.
	 */
	private static File createFromURL(final String fileName,
			final ReplacePluginDialog rpd, boolean temp) {
		File urlFile = cachedURLs.get(fileName);
		if (urlFile != null) {
			if (urlFile.exists()) {
				return urlFile;
			} else {
				cachedURLs.remove(fileName);
			}
		}
		try {
			urlFile = File.createTempFile(TMP_URL_PREFIX, TMP_URL_SUFFIX);
		} catch (IOException e) {
			rpd.setException(e);
			return IO_ERROR_FILE;
		}
		URL url;
		try {
			url = new URL(fileName);
		} catch (MalformedURLException e) {
			// should not happen, since isURL() returned true
			rpd.setException(e);
			return IO_ERROR_FILE;
		}
		URLConnection urlCon;
		try {
			urlCon = url.openConnection();
			urlCon.connect();
		} catch (IOException e) {
			rpd.setException(e);
			return IO_ERROR_FILE;
		}
		final long length = urlCon.getContentLengthLong();

		final long av = 1 + ((length - 1) / Integer.MAX_VALUE);
		final int max = (int) (length / av);
		final File file = urlFile;
		// download thread
		pt = new ProgressThread(KEY + ".download", true) {

			@Override
			public void run() {
				long current;
				int completed;
				ProgressListener pl = getProgressListener();
				pl.setTotal(max);
				pl.setMessage(file.getName());
				do {
					try {
						synchronized (this) {
							wait(200);
						}
					} catch (InterruptedException e) {
					}
					current = file.length();
					completed = (int) (current / av);
					pl.setCompleted(completed);
				} while (current < length);
				pl.complete();
			}

			@Override
			public void executionCancelled() {
				downloader.cancel(true);
			}
		};
		pt.start();

		final URL source = url;
		final File destination = urlFile;
		downloader = new SwingWorker<Void, Void>() {

			@Override
			protected Void doInBackground() throws Exception {
				FileUtils.copyURLToFile(source, destination);
				try {
					URLConnection connection = source.openConnection();
					connection.connect();
					long mod = connection.getLastModified();
					if (mod != 0) {
						destination.setLastModified(mod);
					}
				} catch (Exception e) {
				}
				return null;
			}

		};
		try {
			downloader.execute();
			downloader.get();
			cachedURLs.put(fileName, urlFile);
			return urlFile;
		} catch (InterruptedException e) {
			return null;
		} catch (ExecutionException e) {
			Throwable ioe = e.getCause();
			if (!(ioe instanceof IOException)) {
				return null;
			}
			destination.deleteOnExit();
			rpd.setException((IOException) ioe);
			pt.cancel();
			return IO_ERROR_FILE;
		} catch (CancellationException ce) {
			return null;
		}
	}

	/**
	 * Updates the local cache of URLs and associated files.
	 *
	 * @param oldF
	 *            - The old file to be replaced.
	 * @param newF
	 *            - The new file that replaces the old one.
	 */
	private static void updateCache(File oldF, File newF) {
		for (Entry<String, File> entry : cachedURLs.entrySet()) {
			if (oldF == entry.getValue()) {
				entry.setValue(newF);
				return;
			}
		}
	}

	/**
	 * This class represents the dialog for the {@link ReplaceAction}.
	 *
	 * @author Jan Czogalla
	 *
	 */
	// TODO change to using ButtonDialogBuilder? Could be reduced to content
	// only but not the best idea because of ok method?
	private class ReplacePluginDialog extends ButtonDialog {

		private static final long serialVersionUID = 5764379732811312233L;

		// GUI fields that will be accessed and checked during the loading
		// operation
		private ReplaceAction control;
		private Properties prop;
		private JComboBox<String> plugins;
		private transient Map<String, Plugin> pluginMap;
		private JTextField browseText;
		private JCheckBox isTemp;
		private JCheckBox allowOlder;
		private JCheckBox confirmReplace;
		private JCheckBox startCheck;
		private JCheckBox showResult;
		private JButton checkButton;

		// exception handling for popups, and the download worker
		private Exception exception;
		private transient Plugin plugin;
		private transient SwingWorker<Status, Void> dlWorker;

		/**
		 * Private constructor to build the dialog from the
		 * {@link ReplaceAction}.
		 *
		 * @param control
		 *            - The {@link ReplaceAction} currently executed.
		 * @param pluginID
		 *            - The last id of the extension to be replaced, or
		 *            "autodetect".
		 * @param fileName
		 *            - The last filename used for loading.
		 * @param temp
		 *            - The last setting of "change temporarily".
		 * @param older
		 *            - The last setting of "allow old version".
		 * @param confirm
		 *            - The last setting of "confirm loading".
		 * @param start
		 *            - The last setting of "check on start".
		 * @param showResult
		 *            - The last setting of "show result message"
		 */
		private ReplacePluginDialog(ReplaceAction control, String pluginID,
				String fileName, boolean temp, boolean older, boolean confirm,
				boolean start, boolean showResult) {
			super(RapidMinerGUI.getMainFrame(), KEY + ".replace",
					Dialog.DEFAULT_MODALITY_TYPE, null, new Object[0]);
			this.control = control;
			createPluginList(pluginID);
			createBrowseText(fileName);
			createTempCheck(temp);
			createOlderCheck(older);
			createConfirmCheck(confirm);
			createStartCheck(start);
			createShowResult(showResult);
			layoutDefault(createBox(), checkButton = makeOkButton(KEY
					+ ".check"), makeCancelButton());
		}

		/**
		 * Constructor to build the dialog from the {@link ReplaceAction}. Using
		 * the property object from this extension for convenience.
		 *
		 * @param control
		 *            - The {@link ReplaceAction} currently executed.
		 * @param properties
		 *            - The properties associated with this extension.
		 */
		public ReplacePluginDialog(ReplaceAction control, Properties properties) {
			this(control, properties.getProperty(LAST_PLUGIN), properties
					.getProperty(LAST_FILE), Boolean.valueOf(properties
					.getProperty(LAST_TEMP)), Boolean.valueOf(properties
					.getProperty(LAST_OLD)), Boolean.valueOf(properties
					.getProperty(CONFIRM_REPLACE)), Boolean.valueOf(properties
					.getProperty(LAST_START)), Boolean.valueOf(properties
					.getProperty(SHOW_RESULT)));
			this.prop = properties;
		}

		/**
		 * Creates the list of available extensions for the drop down box. Will
		 * select the previously selected option if possible.
		 *
		 * @param pluginID
		 *            - The last id of the extension to be replaced,
		 *            "autodetect" or null.
		 */
		private void createPluginList(String pluginID) {
			plugins = new JComboBox<String>();
			plugins.setToolTipText(getText("plugin_box.tooltip"));
			pluginMap = new HashMap<String, Plugin>();
			String name;
			boolean loadOld = pluginID != null && !pluginID.trim().isEmpty();
			if (loadOld) {
				pluginID = pluginID.trim();
			}
			String oldPlugin = null;
			plugins.addItem(getText("none_selected.option"));
			for (Plugin p : Plugin.getAllPlugins()) {
				if (p.getExtensionId().endsWith(KEY)) {
					continue;
				}
				name = p.getName();
				pluginMap.put(name, p);
				plugins.addItem(name);
				if (loadOld && p.getExtensionId().equals(pluginID)) {
					oldPlugin = name;
				}
			}
			if (oldPlugin != null) {
				plugins.setSelectedItem(oldPlugin);
			} else {
				plugins.setSelectedIndex(0);
			}
		}

		/**
		 * Creates the browse text field, initialized with the previously used
		 * file.
		 *
		 * @param fileName
		 *            - The last filename used for loading.
		 */
		private void createBrowseText(String fileName) {
			if (fileName == null || fileName.trim().isEmpty()) {
				fileName = "";
			}
			browseText = new JTextField(fileName.trim());
			browseText.setPreferredSize(new Dimension(300, browseText
					.getPreferredSize().height));
		}

		/**
		 * Creates the "load temporarily" checkbox.
		 *
		 * @param temp
		 *            - The last setting of "change temporarily".
		 */
		private void createTempCheck(boolean temp) {
			isTemp = new JCheckBox(getText("temp.label"), temp);
		}

		/**
		 * Creates the "allow older version" checkbox.
		 *
		 * @param older
		 *            - The last setting of "allow old version".
		 */
		private void createOlderCheck(boolean older) {
			allowOlder = new JCheckBox(getText("older.label"), older);
		}

		/**
		 * Creates the "confirm reload" checkbox.
		 *
		 * @param confirm
		 *            - The last setting of "confirm loading".
		 */
		private void createConfirmCheck(boolean confirm) {
			confirmReplace = new JCheckBox(getText("confirm_replace.label"),
					confirm);
		}

		/**
		 * Creates the "check on process start" checkbox.
		 *
		 * @param start
		 *            - The last setting of "check on process start".
		 */
		private void createStartCheck(boolean start) {
			startCheck = new JCheckBox(getText("start.label"), start);
		}

		/**
		 * Creates the "show result" checkbox.
		 *
		 * @param showResult
		 *            - The last setting of "show result".
		 */
		private void createShowResult(boolean show) {
			showResult = new JCheckBox(getText("show_result.label"), show);
		}

		/**
		 * Creates a GUI box containing the different panels.
		 *
		 * @return The GUI box.
		 */
		private Box createBox() {
			Box box = Box.createVerticalBox();
			box.add(createPluginPanel());
			box.add(createBrowsePanel());
			box.add(createPropertyPanel());
			return box;
		}

		/**
		 * Creates the panel containing the GUI elements associated with the
		 * replaced extension.
		 *
		 * @return The extension panel.
		 */
		private JPanel createPluginPanel() {
			JPanel pluginPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			pluginPanel.setAlignmentX(LEFT_ALIGNMENT);
			pluginPanel.setBorder(BorderFactory.createEmptyBorder(0, GAP, 0,
					GAP));
			pluginPanel.add(new FixedWidthLabel(120, getText("replace.label")),
					BorderLayout.WEST);
			pluginPanel.add(plugins, BorderLayout.CENTER);
			return pluginPanel;
		}

		/**
		 * Creates the panel containing the GUI elements associated with the new
		 * extension.
		 *
		 * @return The browse panel.
		 */
		private JPanel createBrowsePanel() {
			JPanel browsePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			browsePanel.setAlignmentX(LEFT_ALIGNMENT);
			browsePanel.setBorder(BorderFactory.createEmptyBorder(0, GAP, 0,
					GAP));
			browsePanel.add(new FixedWidthLabel(120, getText("with.label")),
					BorderLayout.WEST);
			browsePanel.add(browseText, BorderLayout.CENTER);
			browsePanel.add(makeBrowseButton(), BorderLayout.EAST);
			return browsePanel;
		}

		/**
		 * Creates the "browse" button.
		 *
		 * @return The "browse" button.
		 */
		private JButton makeBrowseButton() {
			Action browseAction = new ResourceAction(KEY + ".browse") {

				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					browse(createCurrentDirectory(browseText.getText()));
				}

			};
			return new JButton(browseAction);
		}

		/**
		 * Opens a {@link JFileChooser} to browse the file system for extension
		 * files. Will open at the specified path if possible.
		 *
		 * @param current
		 *            - The last file used for loading.
		 */
		private void browse(File current) {
			JFileChooser jfc = new JFileChooser(current);
			jfc.setFileFilter(new FileNameExtensionFilter(
					"*.jar - Java Archives", "jar"));
			jfc.setAcceptAllFileFilterUsed(false);
			int result = jfc.showOpenDialog(this);
			if (result != JFileChooser.APPROVE_OPTION) {
				return;
			}
			File tmp = jfc.getSelectedFile();
			if (tmp.exists()) {
				browseText.setText(tmp.getAbsolutePath());
			}
		}

		/**
		 * Creates the panel containing the GUI elements associated with the
		 * properties for temporarily loading, older versions and confirmation.
		 *
		 * @return The porperty panel.
		 */
		private JPanel createPropertyPanel() {
			JPanel propertyPanel = new JPanel();
			propertyPanel.setLayout(new BoxLayout(propertyPanel,
					BoxLayout.PAGE_AXIS));
			propertyPanel.setAlignmentX(LEFT_ALIGNMENT);
			propertyPanel.setBorder(BorderFactory.createEmptyBorder(0, GAP, 0,
					GAP));
			propertyPanel.add(isTemp);
			propertyPanel.add(allowOlder);
			propertyPanel.add(confirmReplace);
			propertyPanel.add(startCheck);
			propertyPanel.add(showResult);
			return propertyPanel;
		}

		/**
		 * Executes the file checking and activates either the confirmation for
		 * replacing or just starts the replacing process.
		 */
		@Override
		protected void ok() {
			final String filePath = browseText.getText();
			plugin = pluginMap.get(plugins.getSelectedItem());
			final boolean temp = isTemp.isSelected();
			final boolean older = allowOlder.isSelected();
			dlWorker = new SwingWorker<Status, Void>() {

				@Override
				protected Status doInBackground() throws Exception {
					return control.checkFile(ReplacePluginDialog.this, plugin,
							filePath, temp, older);
				}

				@Override
				public void done() {
					Status result;
					try {
						result = get();
					} catch (Exception e) {
						exception = e;
						result = Status.IO_ERROR;
					}
					Icon oldIcon = checkButton.getIcon();
					if (result != Status.SUCCESS && result != Status.NO_CHANGE) {
						checkButton.setIcon(SwingTools.createIcon("24/"
								+ getText("check.fail")));
						switch (result) {
						case NOT_EXIST:
							SwingTools.showVerySimpleErrorMessage(KEY
									+ ".file_not_exist");
							break;
						case IO_ERROR:
							SwingTools.showSimpleErrorMessage(
									KEY + ".io_error", exception);
							break;
						case NOT_COMPATIBLE:
							SwingTools.showVerySimpleErrorMessage(KEY
									+ ".file_not_compatible");
							break;
						case NOT_DEPLOYABLE:
							SwingTools.showVerySimpleErrorMessage(KEY
									+ ".plugin_not_deployable");
							break;
						case OLD_VERSION:
							SwingTools.showVerySimpleErrorMessage(KEY
									+ ".old_version");
							break;
						case ABORT:
							SwingTools.showMessageDialog(KEY + ".abort");
							break;
						default:
							break;
						}
						lastModified.remove(filePath);
						checkButton.setIcon(oldIcon);
						ReplacePluginDialog.this.setEnabled(true);
						return;
					}
					checkButton.setIcon(SwingTools.createIcon("24/"
							+ getText("check.success")));
					if (result == Status.NO_CHANGE && oldPlugin != null
							&& oldPlugin.getFile().equals(newPluginFile)) {
						SwingTools.showMessageDialog(KEY + ".no_change");
						saveAndDispose();
						return;
					}
					boolean confirm = confirmReplace.isSelected();
					if (confirm) {
						// confirm replace
						int pos = filePath.lastIndexOf(File.separatorChar);
						pos++;
						String fileName = filePath.substring(pos);
						int confirmResult;
						if (plugin == null) {
							confirmResult = SwingTools.showConfirmDialog(KEY
									+ ".new_plugin",
									ConfirmDialog.YES_NO_OPTION, fileName,
									getText("temp." + temp), older ? "" : " "
											+ getText("older.false"));
						} else {
							confirmResult = SwingTools.showConfirmDialog(KEY
									+ ".replace", ConfirmDialog.YES_NO_OPTION,
									plugin.getName(), fileName, getText("temp."
											+ temp), older ? "" : " "
											+ getText("older.false"));
						}
						if (confirmResult == ConfirmDialog.NO_OPTION) {
							control.reset();
							checkButton.setIcon(oldIcon);
							ReplacePluginDialog.this.setEnabled(true);
							return;
						}
					}
					// replace extension
					result = control.replace();
					if (showResult.isSelected()) {
						ideListener.toFront();
					}
					if (result == Status.SUCCESS) {
						// SwingTools.showMessageDialog(KEY + ".success");
						saveAndDispose();
					} else {
						lastModified.remove(filePath);
						SwingTools.showVerySimpleErrorMessage(KEY + ".fail");
						ReplacePluginDialog.this.setEnabled(true);
						reset();
					}
				}
			};
			setEnabled(false);
			dlWorker.execute();
		}

		/**
		 * Saves some settings before disposing the dialog.
		 */
		@Override
		protected void cancel() {
			prop.setProperty(LAST_START, "" + startCheck.isSelected());
			prop.setProperty(SHOW_RESULT, "" + startCheck.isSelected());
			super.cancel();
		}

		/**
		 * Set exception to be shown on error message.
		 *
		 * @param e
		 *            - The exception to be shown.
		 */
		private void setException(Exception e) {
			exception = e;
		}

		/**
		 * Set the the extension to be replaced if it was automatically detected
		 * during
		 * {@link ReplaceAction#checkFile(ReplacePluginDialog, Plugin, String, boolean, boolean)
		 * ReplaceAction.checkFile}.
		 *
		 * @param plugin
		 *            - The extension to be replaced.
		 */
		private void setPlugin(Plugin plugin) {
			this.plugin = plugin;
		}

		@Override
		protected Icon getInfoIcon() {
			return SwingTools.createIcon("48/package_edit.png");
		}

		@Override
		protected String getInfoText() {
			return "";
		}

		/**
		 * Gets the key associated with this dialog to get strings from the GUI
		 * properties.
		 *
		 * @return The key associated with this dialog.
		 */
		private String getDialogKey() {
			String key = super.getKey();
			return key.substring(0, key.lastIndexOf('.'));
		}

		/**
		 * Gets the string associated with this dialog and the specified label.
		 *
		 * @param label
		 *            - The specific key for the wanted string.
		 * @return The string specified by the label.
		 */
		private String getText(String label) {
			return I18N.getMessage(I18N.getGUIBundle(), getDialogKey() + "."
					+ label);
		}

		/**
		 * Save the used options and parameters for the next invocation and
		 * dispose of this dialog.
		 */
		public void saveAndDispose() {
			Plugin p = pluginMap.get(plugins.getSelectedItem());
			prop.setProperty(LAST_PLUGIN, p != null ? p.getExtensionId() : "");
			prop.setProperty(LAST_FILE, browseText.getText());
			prop.setProperty(LAST_TEMP, "" + isTemp.isSelected());
			prop.setProperty(LAST_OLD, "" + allowOlder.isSelected());
			prop.setProperty(CONFIRM_REPLACE, "" + confirmReplace.isSelected());
			prop.setProperty(LAST_START, "" + startCheck.isSelected());
			prop.setProperty(SHOW_RESULT, "" + showResult.isSelected());
			PluginInitIDE2R.saveProperties();
			dispose();
		}

		@Override
		public void setEnabled(boolean enable) {
			super.setEnabled(enable);
			setDefaultCloseOperation(enable ? DISPOSE_ON_CLOSE
					: DO_NOTHING_ON_CLOSE);
		}
	}
}
