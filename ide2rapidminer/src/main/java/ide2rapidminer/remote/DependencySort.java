/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2013-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	http://www-ai.cs.uni-dortmund.de/SOFTWARE/RMD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package ide2rapidminer.remote;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

import com.rapidminer.tools.plugin.Dependency;
import com.rapidminer.tools.plugin.Plugin;

/**
 * This comparator sorts RapidMiner extensions by their dependencies. This is
 * used to find a good order to initialize extensions in a way that they can use
 * any elements and objects created by their dependencies. It's in the extension
 * developers responsibility to prevent circular dependencies.
 *
 * @author Jan Czogalla
 *
 */
public class DependencySort implements Comparator<Plugin>, Serializable {

	private static final long serialVersionUID = -4669768102600619496L;

	/**
	 * Orders two extensions by dependencies. Two extensions are considered
	 * equal and compare returns 0, if neither has dependencies or they do not
	 * depend on each other. If one extension depends on the other, the
	 * dependent extension is considered greater than the extension it depends
	 * on.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public int compare(Plugin pl1, Plugin pl2) {
		if (pl1 == null) {
			if (pl2 == null) {
				return 0;
			}
			return -1;
		}
		if (pl2 == null) {
			return 1;
		}
		if (pl1 == pl2) {
			return 0;
		}
		List<Dependency> l1 = pl1.getPluginDependencies();
		List<Dependency> l2 = pl2.getPluginDependencies();

		if (l1.isEmpty() && l2.isEmpty()) {
			return 0;
		}

		String id1 = pl1.getExtensionId();
		String id2 = pl2.getExtensionId();

		for (Dependency dep : l1) {
			if (dep.getPluginExtensionId().equals(id2)) {
				return 1;
			}
		}

		for (Dependency dep : l2) {
			if (dep.getPluginExtensionId().equals(id1)) {
				return -1;
			}
		}
		return 0;
	}

}
