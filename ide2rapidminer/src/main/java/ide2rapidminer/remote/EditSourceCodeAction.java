/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2013-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	http://www-ai.cs.uni-dortmund.de/SOFTWARE/RMD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package ide2rapidminer.remote;

import static com.rapidminer.PluginInitIDE2R.KEY;
import static ide2rapidminer.interfaces.Constants.RMI_PORT_RM2E;
import static ide2rapidminer.interfaces.OpenAndActivate.OPENER;
import ide2rapidminer.interfaces.OpenAndActivate;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.ServerException;
import java.rmi.UnmarshalException;
import java.rmi.registry.LocateRegistry;

import com.rapidminer.gui.MainFrame;
import com.rapidminer.gui.tools.ResourceAction;
import com.rapidminer.gui.tools.SwingTools;
import com.rapidminer.operator.Operator;

/**
 * The action class to edit the source code of a selected RapidMiner
 * {@link Operator}.
 * 
 * @author Jan Czogalla
 * @author Daniel Smit
 * 
 */
public class EditSourceCodeAction extends ResourceAction {

	private static final long serialVersionUID = 6514541077380093077L;

	private MainFrame mainFrame;

	/**
	 * Creates the action and registers the mainframe.
	 * 
	 * @param mainFrame
	 *            - the RapidMiner mainframe.
	 */
	public EditSourceCodeAction(MainFrame mainFrame) {
		super(true, KEY + ".editsource");
		this.mainFrame = mainFrame;
		setCondition(OPERATOR_SELECTED, MANDATORY);
	}

	/**
	 * Tries to open the source code of the selected {@link Operator} in the
	 * IDE. Upon success, the RapidMiner {@link MainFrame} will be minimized.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO: only first selected operator or all selected operators?
		// boolean success = false;
		// for (Operator op : mainFrame.getSelectedOperators()) {
		Operator op = mainFrame.getFirstSelectedOperator();
		if (op == null) {
			SwingTools.showMessageDialog(KEY + ".no_op");
			return;
		}
		String className = op.getClass().getName();
		String namespace = op.getOperatorDescription().getProviderNamespace();
		boolean isOpened;
		try {
			OpenAndActivate oaa = (OpenAndActivate) LocateRegistry.getRegistry(
					RMI_PORT_RM2E).lookup(OPENER);
			try {
				isOpened = oaa.open(className, namespace);
			} catch (ServerException se) {
				if (se.getCause() instanceof UnmarshalException) {
					// backward compatibility to 1.0.1
					isOpened = oaa.open(className);
				} else {
					throw se;
				}
			}
			if (!isOpened) {
				SwingTools.showMessageDialog(KEY + ".not_editable");
				return;
				// break;
			}
		} catch (RemoteException re) {
			SwingTools.showSimpleErrorMessage(KEY + ".remote", re);
			return;
			// break;
		} catch (NotBoundException nbe) {
			SwingTools.showSimpleErrorMessage(KEY + ".remote", nbe);
			return;
			// break;
		}
		// success = true;
		// }
		// if (success) {
		int state = mainFrame.getExtendedState();
		state |= Frame.ICONIFIED;
		mainFrame.setExtendedState(state);
		// }
	}
}
