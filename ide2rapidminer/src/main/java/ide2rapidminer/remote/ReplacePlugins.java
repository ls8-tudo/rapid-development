/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2013-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	http://www-ai.cs.uni-dortmund.de/SOFTWARE/RMD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package ide2rapidminer.remote;

import static com.rapidminer.PluginInitIDE2R.KEY;
import ide2rapidminer.interfaces.ReloadOperators;
import ide2rapidminer.util.Compatibility;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

import javax.swing.JMenu;
import javax.swing.JPopupMenu;

import com.rapidminer.FileProcessLocation;
import com.rapidminer.Process;
import com.rapidminer.ProcessLocation;
import com.rapidminer.gui.MainFrame;
import com.rapidminer.gui.RapidMinerGUI;
import com.rapidminer.gui.tools.ResourceAction;
import com.rapidminer.gui.tools.ResourceActionAdapter;
import com.rapidminer.gui.tools.ResourceMenu;
import com.rapidminer.gui.tools.SwingTools;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorChain;
import com.rapidminer.tools.LogService;
import com.rapidminer.tools.plugin.Dependency;
import com.rapidminer.tools.plugin.Plugin;

/**
 * The implementation of the remote interface {@link ReloadOperators}. This is
 * the core for (re)registering RapidMiner extensions at runtime.
 *
 * @author Jan Czogalla
 * @author Daniel Smit
 *
 */
public class ReplacePlugins implements ReloadOperators {

	/**
	 * The release folder in the RapidMiner project folder if started from an
	 * IDE.
	 */
	private static final String RELEASE = "release";
	/**
	 * The default location of a built extension, considering that RapidMiner is
	 * started from an IDE as a Java application or from the jar file.
	 */
	private static final String DEFAULT_COPY_FROM_LOCATION = getDefaultPath();

	// fields used for copying
	private static final int MAX_BYTES = 15 * 1024 * 1024;
	private Set<String> toCopy = new HashSet<String>();

	// fields used for control
	private MainFrame mainFrame;
	private List<String> failed;
	private File tmpProcess;
	private Runnable showResults;

	/**
	 * Determines the default location where extensions will be constructed when
	 * running the ant build script. This is only relevant if RapidMiner is run
	 * from eclipse and should result in the release folder of the RapidMiner
	 * project. This is used to resolve relative paths during reload.
	 *
	 * @return the default temporary location for extensions.
	 */
	private static String getDefaultPath() {
		// get the current execution environment
		String path = RapidMinerGUI.class.getProtectionDomain().getCodeSource()
				.getLocation().getPath();
		try {
			path = URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return "";
		}
		if (path.endsWith(".jar")) {
			// jar file is found in lib folder
			path = new File(path).getParentFile().getParent();
		} else {
			// usually is run from default build folder
			path = new File(path).getParent();
		}
		return path + File.separator + RELEASE;
	}

	/**
	 * The constructor registers the RapidMiner mainframe so the RapidMiner GUI
	 * can be manipulated and restored.
	 *
	 * @param mainFrame
	 *            - the RapidMiner mainframe
	 */
	public ReplacePlugins(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
	}

	@Override
	public boolean reload(Map<String, String> namespaces) {
		LogService.getRoot().fine("RapidMiner Reload-Action");
		setFinalFiles(namespaces);
		if (namespaces.isEmpty()) {
			LogService.getRoot().info("No extensions to reload.");
			return true;
		}

		addDependentPlugins(namespaces);

		// save current process
		Process process = mainFrame.getProcess();
		boolean hasOps = false;
		// check all operators for need to update
		for (Operator op : process.getAllOperators()) {
			if (namespaces.keySet().contains(
					op.getOperatorDescription().getProviderNamespace())) {
				hasOps = true;
				break;
			}
		}

		boolean hadSaveLoc = process.hasSaveDestination();
		ProcessLocation procLoc;

		String currentChain = Compatibility.getDefault().getDisplayedChain()
				.getName();

		try {
			procLoc = saveProcess(hasOps, hadSaveLoc, process);
		} catch (IOException e) {
			// could not save process
			// TODO dummy handling
			LogService.getRoot().warning("Save process failed.");
			e.printStackTrace();
			return false;
		}

		failed = new ArrayList<String>();
		// copy files
		copyMarkedFiles(namespaces);

		// remove ide2rm from mapping after copying because it can not be
		// replaced at runtime
		if (namespaces.containsKey("rmx_" + KEY)) {
			namespaces.remove("rmx_" + KEY);
			if (namespaces.isEmpty()) {
				LogService.getRoot().info("No extensions to reload.");
				return true;
			}
		}

		// tear down extensions, but remember them to reload them if something
		// goes wrong
		Plugin p;
		Map<String, Plugin> oldPlugins = new HashMap<String, Plugin>();
		for (String pluginID : namespaces.keySet()) {
			p = Plugin.getPluginByExtensionId(pluginID);
			if (p != null) {
				oldPlugins.put(pluginID, p);
				p.tearDown();
				try {
					// free native libraries
					Field natLibs = ClassLoader.class
							.getDeclaredField("nativeLibraries");
					natLibs.setAccessible(true);
					natLibs.set(p.getClassLoader(), new Vector<>());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		// reload extensions
		Plugin plugin;
		String jarName;
		List<Plugin> plugins = new ArrayList<Plugin>();
		for (Entry<String, String> nsEntry : namespaces.entrySet()) {
			plugin = null;
			try {
				jarName = nsEntry.getValue();
				plugin = new Plugin(new File(jarName));
			} catch (IOException e) {
				e.printStackTrace();
				failed.add(nsEntry.getKey());
				// new extension can not be loaded
				// load old file instead...
				plugin = oldPlugins.get(nsEntry.getKey());
				if (plugin == null) {
					// ...or go to next
					continue;
				}
			}
			plugins.add(plugin);
			LogService.getRoot().info("Reloading plugin " + nsEntry.getKey());
			plugin.reregister();
		}
		oldPlugins.clear();

		// finish extension initialization
		Collections.sort(plugins, new DependencySort());
		boolean success = false;
		for (Plugin pl : plugins) {
			LogService.getRoot().info(
					"Finishing registration of plugin " + pl.getExtensionId());
			try {
				pl.finishReregister();
				success = true;
			} catch (NullPointerException npe) {
				// extension is not fully functional!
				failed.add(pl.getName());
			}
		}

		// reload process
		reloadProcess(hasOps, hadSaveLoc, procLoc, currentChain);

		// refresh "about extensions" menu
		refreshAbout();

		StringBuilder result = new StringBuilder();
		if (!plugins.isEmpty()) {
			result.append("The following extensions were successfully loaded:<br>");
			for (Plugin pl : plugins) {
				result.append(pl.getName() + "<br>");
			}
		}
		if (!failed.isEmpty()) {
			result.append("The following extensions could not be copied or loaded:<br>");
			for (String rmx : failed) {
				result.append(rmx + "<br>");
			}
		}

		if (success) {
			final String showResult = result.toString();
			showResults = new Runnable() {

				@Override
				public void run() {
					SwingTools.showMessageDialog(KEY + ".replace_result",
							showResult);
				}
			};
		}
		return success;
	}

	@Override
	public void toFront() {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				int state = mainFrame.getExtendedState();
				state &= ~Frame.ICONIFIED;
				mainFrame.setExtendedState(state);
				if (showResults != null) {
					showResults.run();
					showResults = null;
				}
				// mainFrame.setVisible(true);
				// mainFrame.setAlwaysOnTop(true);
				// mainFrame.toFront();
				// mainFrame.requestFocus();
				// mainFrame.repaint();
				// mainFrame.setAlwaysOnTop(false);
			}
		});
	}

	/**
	 * Sets the final file names in the given map. Will convert relative paths
	 * to absolute paths.
	 *
	 * @param namespaces
	 *            - The mapping of old extensions to new jar files.
	 */
	private void setFinalFiles(Map<String, String> namespaces) {
		String fileName;
		for (String id : new HashSet<String>(namespaces.keySet())) {
			fileName = namespaces.get(id);
			if (isLocal(fileName)) {
				fileName = convertFromLocal(fileName, id);
				if (fileName == null) {
					namespaces.remove(id);
				} else {
					namespaces.put(id, fileName);
				}
			}
		}
	}

	/**
	 * Checks if a given file is local or not. A file is considered "local" if
	 * it does not exist in its current location.
	 *
	 * @param fileName
	 *            - The file name to be checked.
	 * @return true if the given file name is local.
	 */
	private boolean isLocal(String fileName) {
		return !new File(fileName).exists();
	}

	/**
	 * Tries to convert a local file into an absolute path. Possible locations
	 * for local files are the default temporary extension folder (release) and
	 * the RapidMiner extension location.
	 *
	 * @param fileName
	 *            - The file name to be converted.
	 * @param extensionID
	 *            - The ID of the extension to check against if necessary.
	 * @return the absolute file name if it exists, or null otherwise.
	 */
	private String convertFromLocal(String fileName, final String extensionID) {
		File defCopyLoc = new File(DEFAULT_COPY_FROM_LOCATION);
		File pluginLoc = null;
		try {
			pluginLoc = Plugin.getPluginLocation();
		} catch (IOException e) {
		}
		// check for extension in release folder
		File file = new File(defCopyLoc, fileName);
		if (!file.exists() && pluginLoc != null) {
			// check for extension in plugin folder
			file = new File(pluginLoc, fileName);
		}
		if (!file.exists()) {
			// check release and plugin folder for matching file
			FileFilter filter = new FileFilter() {

				@Override
				public boolean accept(File pathname) {
					if (!pathname.getName().endsWith(".jar")) {
						return false;
					}
					Plugin p;
					try {
						p = new Plugin(pathname);
					} catch (IOException e) {
						return false;
					}
					return p.getExtensionId().equals(extensionID);
				}
			};
			long timestamp = 0;
			File[] files = defCopyLoc.listFiles(filter);
			if (files != null) {
				for (File f : files) {
					if (f.lastModified() > timestamp) {
						timestamp = f.lastModified();
						file = f;
					}
				}
			}
			if (pluginLoc != null) {
				files = pluginLoc.listFiles(filter);
				if (files != null) {
					for (File f : files) {
						if (f.lastModified() > timestamp) {
							timestamp = f.lastModified();
							file = f;
						}
					}
				}
			}
		}
		return file.exists() ? file.getAbsolutePath() : null;
	}

	/**
	 * Adds all extensions that depend on already marked extensions to the list
	 * of marked extensions. The initial list consists of the changed
	 * extensions. Dependent extensions must be reloaded as well, although their
	 * jar files did not change, because they use the class loaders of the
	 * changed extensions.
	 *
	 * @param namespaces
	 *            - the mapping of changed extensions.
	 */
	private void addDependentPlugins(Map<String, String> namespaces) {
		List<Plugin> plugins = new ArrayList<Plugin>(Plugin.getAllPlugins());
		boolean change = true;
		Plugin plugin;
		List<Dependency> deps;
		while (change && !plugins.isEmpty()) {
			change = false;
			Iterator<Plugin> iter = plugins.iterator();
			while (iter.hasNext()) {
				plugin = iter.next();
				deps = plugin.getPluginDependencies();
				for (Dependency dep : deps) {
					// ignore ide2rm since it can not be reloaded at runtime
					if (dep.getPluginExtensionId().endsWith(KEY)) {
						continue;
					}
					if (namespaces.containsKey(dep.getPluginExtensionId())) {
						change = true;
						namespaces.put(plugin.getExtensionId(), plugin
								.getFile().getAbsolutePath());
						iter.remove();
						break;
					}
				}
			}
		}

	}

	/**
	 * Saves the currently opened process in a temporary file. Returns the
	 * actual save location of the process if it has one.
	 *
	 * @param hasOps
	 *            - Indicates if the process must be saved.
	 * @param hadSaveLoc
	 *            - Denotes if the process had a save location.
	 * @param process
	 *            - The process to be saved.
	 * @return The process location associated with the current process or null
	 *         if the process has no process location.
	 * @throws IOException
	 *             if a file error occurs.
	 */
	private ProcessLocation saveProcess(boolean hasOps, boolean hadSaveLoc,
			Process process) throws IOException {
		if (!hasOps) {
			return null;
		}
		ProcessLocation processFile = null;
		if (hadSaveLoc) {
			processFile = process.getProcessLocation();
		}
		tmpProcess = File.createTempFile("RMP", ".xml");
		tmpProcess.deleteOnExit();
		process.setProcessLocation(new FileProcessLocation(tmpProcess));
		process.stop();

		process.save();

		// replace current process with an empty one
		mainFrame.setProcess(new Process(), false);
		LogService.getRoot().info("Saved process.");
		if (hadSaveLoc) {
			return processFile;
		} else {
			return null;
		}
	}

	/**
	 * Copies the previously marked jar files to update the extensions. Will
	 * first try to do so with OS specific commands and on fail tries again in a
	 * byte wise fashion.
	 *
	 * @param namespaces
	 *            - The mapping of files that may need copying.
	 *
	 * @return true if (some of) the files were copied.
	 */
	private boolean copyMarkedFiles(Map<String, String> namespaces) {
		if (toCopy.isEmpty()) {
			return true;
		}
		Path pl, jar;
		String path, file;
		boolean success = false, tmp;
		for (String id : toCopy) {
			tmp = false;
			path = namespaces.get(id);
			file = new File(path).getName();
			jar = Paths.get(path);
			try {
				pl = Paths.get(Plugin.getPluginLocation().getAbsolutePath(),
						file);
			} catch (IOException e) {
				pl = Paths.get(file);
			}
			if (jar.equals(pl)) {
				continue;
			}
			try {
				Files.copy(jar, pl, StandardCopyOption.REPLACE_EXISTING);
				tmp = true;
			} catch (IOException ioe) {
				tmp = dirtyCopy(pl, jar);
			}
			if (tmp) {
				if (path.startsWith(DEFAULT_COPY_FROM_LOCATION)) {
					new File(path).deleteOnExit();
				}
				namespaces.put(id, pl.toString());
			} else {
				failed.add(id);
			}
			success |= tmp;
		}
		toCopy.clear();
		return success;
	}

	/**
	 * Copies data from the given jar to the specified path byte wise.
	 *
	 * @param pl
	 *            - The extension destination.
	 * @param jar
	 *            - The newly created jar file.
	 * @return - true if copying was successful.
	 */
	private boolean dirtyCopy(Path pl, Path jar) {
		File f = pl.toFile();
		if (!f.canWrite()) {
			return false;
		}
		File newF = jar.toFile();
		if (!newF.exists()) {
			return false;
		}
		FileInputStream fip;
		try {
			fip = new FileInputStream(newF);
		} catch (FileNotFoundException e) {
			return false;
		}
		long length = newF.length();
		int partLength = MAX_BYTES;
		if (length < partLength) {
			partLength = (int) length;
		}
		byte[] data = new byte[partLength];
		FileOutputStream fop = null;
		try {
			int curLength;
			fop = new FileOutputStream(f);
			while (length > 0) {
				curLength = fip.read(data);
				fop.write(data, 0, curLength);
				fop.flush();
				length -= curLength;
			}
		} catch (IOException e) {
			LogService.getRoot().warning("Problem occured while copying " + f);
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
			}
			try {
				fip.close();
			} catch (IOException e) {
			}
		}
		return true;
	}

	/**
	 * Reloads the temporarily saved process.
	 *
	 * @param hasOps
	 *            - Indicates if reloading is necessary.
	 * @param hadSaveLoc
	 *            - Denotes if the process had a save location before.
	 * @param procLoc
	 *            - The save location of the process or null if not existent.
	 * @param currentChain
	 *            - The name of the operator chain that was viewed when the
	 *            process was saved.
	 *
	 */
	private void reloadProcess(boolean hasOps, boolean hadSaveLoc,
			ProcessLocation procLoc, String currentChain) {
		if (!hasOps) {
			return;
		}
		// String userDir = System.getProperty("java.io.tmpdir");
		// File f = new File(userDir, "tmpProcess.xml");
		ProcessLocation tmp;
		tmp = new FileProcessLocation(tmpProcess);
		// reset temporary file
		tmpProcess = null;
		try {
			// reload process
			Process process = new Process(tmp.getRawXML());

			RapidMinerGUI.getRecentFiles().remove(tmp);
			mainFrame.updateRecentFileList();
			if (hadSaveLoc) {
				process.setProcessLocation(procLoc);
			}
			LogService.getRoot().info("Reloading process.");
			mainFrame.setProcess(process, false);
			// set the view to the last seen operator chain
			if (currentChain != null) {
				Compatibility.getDefault().setDisplayedChain(
						(OperatorChain) process.getOperator(currentChain));
			}
		} catch (Exception e) {
			SwingTools.showSimpleErrorMessage(KEY + ".process_not_loaded", e);
		}
	}

	/**
	 * Recreates the menu showing the details of the installed and active
	 * extensions.
	 */
	private void refreshAbout() {
		// TODO help menu vs extensions menu
		JPopupMenu helpMenu = mainFrame.getHelpMenu().getPopupMenu();
		ResourceMenu extensionsMenu = null;
		ResourceActionAdapter a;

		for (Component comp : helpMenu.getComponents()) {
			if (!(comp instanceof JMenu)) {
				continue;
			}
			extensionsMenu = (ResourceMenu) comp;
			a = (ResourceActionAdapter) extensionsMenu.getAction();
			if (a.getKey().equals("menu." + "about_extensions")) {
				break;
			}
			extensionsMenu = null;
		}

		if (extensionsMenu == null) {
			return;
		}

		extensionsMenu.removeAll();
		List<Plugin> allPlugins = Plugin.getAllPlugins();
		if (allPlugins.size() > 0) {
			for (final Plugin plugin : allPlugins) {
				if (plugin.showAboutBox()) {
					extensionsMenu.add(new ResourceAction("about_extension",
							plugin.getName()) {

						private static final long serialVersionUID = 1L;

						@Override
						public void actionPerformed(ActionEvent e) {
							plugin.createAboutBox(mainFrame).setVisible(true);
						}
					});
				}
			}
		}
	}

	@Override
	public void markForCopy(String pluginID) {
		if (pluginID == null) {
			return;
		}
		toCopy.add(pluginID);
	}

}
