/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * Contributors:
 *     Jan Czogalla, Daniel Smit - initial API and implementation
 ******************************************************************************/

package ide2rapidminer.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;

/**
 * Remote interface for replacing/reloading extensions in RapidMiner.
 *
 * @author Jan Czogalla, Daniel Smit
 *
 */
public interface ReloadOperators extends Remote {

	/**
	 * The reference string for finding an object of this interface in the RMI
	 * registry
	 */
	public static final String RELOADER = "reload";

	/**
	 * Replaces extensions in RapidMiner using the given mapping. Old extension
	 * IDs are mapped against the name of the jar files to replace the old
	 * extensions. Returns true if at least one extension was successfully
	 * reloaded. Will show a pop-up in RapidMiner, which extensions were loaded
	 * and which not.
	 *
	 * @param namespaces
	 *            - The mapping of old extensions to new jar files.
	 * @return true if the operation succeeded.
	 * @throws RemoteException
	 *             - if an RMI error occurred.
	 */
	public boolean reload(Map<String, String> namespaces)
			throws RemoteException;

	/**
	 * Restores the RapidMiner window.
	 */
	public void toFront() throws RemoteException;

	/**
	 * Marks the specified extension for copying. The content of the new jar
	 * file will be copied after all relevant extensions were torn down and
	 * before reloading. The newly created file will then be deleted on
	 * RapidMiner shutdown at the latest.
	 *
	 * @param pluginID
	 *            - The id of the extension which jar file must be copied.
	 * @throws RemoteException
	 *             - if an RMI error occurred.
	 */
	public void markForCopy(String pluginID) throws RemoteException;

}
