/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * Contributors:
 *     Jan Czogalla, Daniel Smit - initial API and implementation
 ******************************************************************************/

package ide2rapidminer.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Remote interface for opening a source file and activating the IDE to show it.
 *
 * @author Jan Czogalla, Daniel Smit
 *
 */
public interface OpenAndActivate extends Remote {

	/**
	 * The reference string for finding an object of this interface in the RMI
	 * registry
	 */
	public static final String OPENER = "open";

	/**
	 * Opens the source for the specified class in the IDE and brings the IDE to
	 * the front. Will return true if and only if the class exists in any
	 * RapidMiner extension in the workspace or is a core class and the source
	 * file could be opened.
	 *
	 * @param clazz
	 *            The name of the class to edit in the IDE.
	 * @return true if opening the source file was successful.
	 * @throws RemoteException
	 *             if an RMI error occurred.
	 */
	public boolean open(String clazz) throws RemoteException;

	/**
	 * Opens the source for the specified class in the IDE and brings the IDE to
	 * the front. Will return true if and only if the class exists in the
	 * RapidMiner extension specified by the id in the workspace or is a core
	 * class and the source file could be opened.
	 *
	 * @param clazz
	 *            The name of the class to edit in the IDE.
	 * @param id
	 *            The id of the RapidMiner extension that should contain the
	 *            class.
	 * @return true if opening the source file was successful.
	 * @throws RemoteException
	 *             if an RMI error occurred.
	 */
	public boolean open(String clazz, String id) throws RemoteException;
}
