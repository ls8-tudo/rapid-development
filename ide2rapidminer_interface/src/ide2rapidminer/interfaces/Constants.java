/*******************************************************************************
 * Copyright (c) 2013-2016 TU Dortmund, Computer Science VIII.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * Contributors:
 *     Jan Czogalla, Daniel Smit - initial API and implementation
 ******************************************************************************/

package ide2rapidminer.interfaces;

/**
 * Non-instantiable class holding shared constants for the RapidMiner
 * development extensions for RapidMiner and the IDE. Cannot be extended.
 *
 * @author Jan Czogalla, Daniel Smit
 *
 */
public final class Constants {

	/*						  */
	/* Ant specific constants */
	/*						  */

	/**
	 * The extension of the ant build file for RapidMiner extensions.
	 */
	public static final String ANT_BUILD_FILE_SUFFIX = "xml";

	/**
	 * The name of the ant property file.
	 */
	public static final String ANT_PROPERTY_FILE = "build.properties";

	/**
	 * The XML tag for properties in above build files.
	 */
	public static final String ANT_PROPERTY = "property";

	/**
	 * The ant property name for the RapidMiner directory.
	 */
	public static final String ANT_RM_DIR = "rm.dir";

	/**
	 * Property name of the extension version.
	 */
	public static final String ANT_EXT_VERSION = "extension.version";
	/**
	 * Property name of the extension revision.
	 */
	public static final String ANT_EXT_REVISION = "extension.revision";
	/**
	 * Property name of the extension update.
	 */
	public static final String ANT_EXT_UPDATE = "extension.update";

	/**
	 * Property name of the extension name.
	 */
	public static final String ANT_EXT_NAME = "extension.name";
	/**
	 * Property name of the extension namespace.
	 */
	public static final String ANT_EXT_NAMESPACE = "extension.namespace";

	/**
	 * The fail indicator string for ant builds.
	 */
	public static final String ANT_FAILED = "BUILD FAILED";

	/**
	 * The indicator string for manual copying.
	 */
	public static final String ANT_NOT_MOVED = "Unable to remove existing file";

	/*						     */
	/* Gradle specific constants */
	/*						     */

	/**
	 * The name of the gradle build file.
	 */
	public static final String GRADLE_BUILD_FILE = "build.gradle";

	/**
	 * The name of the gradle property file.
	 */
	public static final String GRADLE_PROPERTY_FILE = "gradle.properties";

	/**
	 * The id of the RapidMiner gradle plugin
	 */
	public static final String GRADLE_RM_PLUGIN = "com.rapidminer.extension";

	/**
	 * The name of the extension configuration block in the gradle build file.
	 */
	public static final String GRADLE_EXT_CONF = "extensionConfig";

	/**
	 * The name of the version in the gradle build file or property file.
	 */
	public static final String GRADLE_EXT_VERSION = "version";

	/**
	 * The name of the extension inside the extension configuration block in the
	 * gradle build file.
	 */
	public static final String GRADLE_EXT_NAME = "name";

	/**
	 * The namespace of the extension inside the extension configuration block
	 * in the gradle build file.
	 */
	public static final String GRADLE_EXT_NAMESPACE = "namespace";

	/*						 */
	/* RM specific constants */
	/*						 */

	/**
	 * The fully qualified RapidMiner main class name.
	 */
	public static final String RM_MAIN_CLASS = "com.rapidminer.RapidMiner";

	/**
	 * A list of compatible RapidMiner versions (suffixes), in descending order
	 * of preference.
	 */
	public static final String RM_MIN_VERSION = "5.3";

	/*						  */
	/* RMI specific constants */
	/*						  */

	/**
	 * The port of the registry from the RM2E-plugin.
	 */
	public static final int RMI_PORT_RM2E = 44332;

	/**
	 * The port of the registry from the IDE2RM-plugin.
	 */
	public static final int RMI_PORT_IDE2RM = 44333;

	private Constants() {
		// private default constructor to prevent instantiation
	}

}
